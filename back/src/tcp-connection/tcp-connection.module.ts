import { Module } from '@nestjs/common';
import { TcpConnectionController } from './tcp-connection.controller';
import { TcpConnectionService } from './tcp-connection.service';
import { TcpConnectionSchema } from './schemas/tcpConnection.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DeviceModule } from '../device/device.module';
import { DeviceService } from '../device/device.service';

@Module({
  imports: [
    DeviceModule,
    MongooseModule.forFeature([
      {
        name: 'TcpConnection',
        schema: TcpConnectionSchema,
        collection: 'tcpConnection',
      },
    ]),
  ],
  controllers: [TcpConnectionController],
  providers: [TcpConnectionService, DeviceService],
  exports: [TcpConnectionService],
})
export class TcpConnectionModule {}
