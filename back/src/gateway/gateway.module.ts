import { Module } from '@nestjs/common';
import { GatewayController } from './gateway.controller';
import { GatewayService } from './gateway.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GatewaySchema } from './schemas/gateway.schema';
import { DeviceModule } from '../device/device.module';
import { DeviceService } from '../device/device.service';

@Module({
  imports: [
    DeviceModule,
    MongooseModule.forFeature([
      { name: 'Gateway', schema: GatewaySchema, collection: 'gateway' },
    ]),
  ],
  controllers: [GatewayController],
  providers: [GatewayService, DeviceService],
  exports: [GatewayService],
})
export class GatewayModule {}
