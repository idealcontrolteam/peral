export class CreateGatewayDTO {
  name: string;
  description: string;
  active: boolean;
  workPlaceId: string;
}
