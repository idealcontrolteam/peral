import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateZoneDTO } from './dto/zone.dto';
import { ZoneService } from './zone.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('zone')
//@UseGuards(new AuthGuard())
export class ZoneController {
  constructor(private zoneService: ZoneService) {}

  @Post()
  async createZone(@Res() res, @Body() body: CreateZoneDTO) {
    if (!body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Work place id is not a valid ObjectId');
    }

    const newZone = await this.zoneService.createZone(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Zone created successfully',
      data: newZone,
    });
  }

  @Get()
  async getZones(@Res() res) {
    const zones = await this.zoneService.getZones();

    let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: zones,
      count: zones.length,
    });
  }

  @Get("/name/:name")
  async getNameZones(@Res() res, @Param('name') name) {
    const zones = await this.zoneService.getNameZones(name);

    let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: zones,
      count: zones.length,
    });
  }

  @Get('/all')
  async getZonesAll(@Res() res) {
    const zones = await this.zoneService.getZonesAll();

    let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: zones,
      count: zones.length,
    });
  }

  @Get('/getZoneWithTagsAndMeasurements/:zoneId')
  async getZoneWithTagsAndMeasurements(@Res() res, @Param('zoneId') zoneId) {
    const tags = await this.zoneService.getZoneWithTagsAndMeasurements(zoneId);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:zoneId/location')
  async getLocationsByZoneId(@Res() res, @Param('zoneId') zoneId) {
    if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Zone id is not a valid ObjectId');
    }

    const locations = await this.zoneService.getLocationsByZoneId(zoneId);

    let msg =
      locations.length == 0 ? 'Locations not found' : 'Locations fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: locations,
      count: locations.length,
    });
  }

  @Get('/:zoneId/tag')
  async getTagsByZoneId(@Res() res, @Param('zoneId') zoneId) {
    if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Zone id is not a valid ObjectId');
    }
   // console.log(zoneId);
    const tags = await this.zoneService.getTagsByZoneId(zoneId);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length
    });
  }

  @Get('/:zoneId')
  async getZone(@Res() res, @Param('zoneId') zoneId) {
    if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Zone id is not a valid ObjectId');
    }

    const zone = await this.zoneService.getZone(zoneId);
    if (!zone) {
      throw new NotFoundException('Zone not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Zone found',
      data: zone,
    });
  }

  @Put('/:zoneId')
  async updateZone(
    @Res() res,
    @Body() body: CreateZoneDTO,
    @Param('zoneId') zoneId,
  ) {
    if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Zone id is not a valid ObjectId');
    }

    const updatedZone = await this.zoneService.updateZone(zoneId, body);
    if (!updatedZone) {
      throw new NotFoundException('Zone not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Zone updated',
      data: updatedZone,
    });
  }

  @Delete('/:zoneId')
  async deleteZone(@Res() res, @Param('zoneId') zoneId) {
    const deletedZone = await this.zoneService.deleteZone(zoneId);

    if (!deletedZone) {
      throw new NotFoundException('Zone not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Zone deleted',
      data: deletedZone,
    });
  }
}
