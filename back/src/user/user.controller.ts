import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { AuthGuard } from 'src/shared/auth.guard';


@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  //@UseGuards(new AuthGuard())
  async createUser(@Res() res, @Body() body: CreateUserDTO) {
    const newUser = await this.userService.createUser(body);
    newUser.password = '';
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'User created successfully',
      data: newUser,
    });
  }

  @Post('/login')
  async login(@Res() res, @Body() body) {
    //console.log(body);
    const userDB = await this.userService.login(body);
    if (!userDB || !(await bcrypt.compare(body.password, userDB.password))) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }

    userDB.lastConnection = new Date();
    const userUpdated = await userDB.save();

    const token = this.getToken(userUpdated);
    const updateToken=await this.userService.updateUserToken(userDB._id,{"token":token});
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'User loged in successfuly',
      data: {
        rol:userDB.roleId,
        user: userUpdated.name,
        token: token,
        expiresIn:  60 * 1000,
      },
    });
  }

  private getToken(user) {
    const { id, username, email, roleId } = user;
    return jwt.sign({ id, username, email, roleId }, 'ULTRASMEGAECRETO', {
      expiresIn: 60,
    });
  }

  @Get('/check_token/:token')
  //@UseGuards(new AuthGuard())
  async checkToken(@Res() res, @Param('token') token) {
    //console.log(token)
    var decoded = jwt.decode(token);
    //console.log(decoded);
    const user = await this.userService.checkToken(decoded);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    if(user.token!=token){
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'NO',
        data: user,
      });
    }else{
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'User found',
        data: user,
      });
    }
  }

  
  @Get()
  //@UseGuards(new AuthGuard())
  async getUsers(@Res() res) {
    const users = await this.userService.getUsers();

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/all')
  //@UseGuards(new AuthGuard())
  async getUsersAll(@Res() res) {
    const users = await this.userService.getUsersAll();

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/:userId')
  //@UseGuards(new AuthGuard())
  async getUser(@Res() res, @Param('userId') userId) {
    if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Id is not a valid ObjectId');
    }

    const user = await this.userService.getUser(userId);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'User found',
      data: user,
    });
  }

  @Put('/:userId')
  //@UseGuards(new AuthGuard())
  async updateUser(
    @Res() res,
    @Body() body: CreateUserDTO,
    @Param('userId') userId,
  ) {
    if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Id is not a valid  ObjectId');
    }
    const updatedUser = await this.userService.updateUser(userId, body);
    if (!updatedUser) {
      throw new NotFoundException('User not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'User updated',
      data: updatedUser,
    });
  }

  @Delete('/:userId')
  //@UseGuards(new AuthGuard())
  async deleteUser(@Res() res, @Param('userId') userId) {
    const deletedUser = await this.userService.deleteUser(userId);

    if (!deletedUser) {
      throw new NotFoundException('User not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'User deleted',
      data: deletedUser,
    });
  }
}
