import { Module } from '@nestjs/common';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanySchema } from './schemas/company.schema';
import { WorkPlaceModule } from '../work-place/work-place.module';
import { WorkPlaceService } from '../work-place/work-place.service';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';

@Module({
  imports: [
    WorkPlaceModule,
    UserModule,
    MongooseModule.forFeature([
      { name: 'Company', schema: CompanySchema, collection: 'company' },
    ]),
  ],
  controllers: [CompanyController],
  providers: [CompanyService, WorkPlaceService, UserService],
})
export class CompanyModule {}
