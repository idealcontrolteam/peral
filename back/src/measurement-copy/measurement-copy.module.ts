import { Module } from '@nestjs/common';
import { MeasurementCopyController } from './measurement-copy.controller';
import { MeasurementCopyService } from './measurement-copy.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementCopySchema } from './schemas/measurementCopy.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'MeasurementCopy',
        schema: MeasurementCopySchema,
        collection: 'measurementCopy',
      },
    ]),
  ],
  controllers: [MeasurementCopyController],
  providers: [MeasurementCopyService],
})
export class MeasurementCopyModule {}
