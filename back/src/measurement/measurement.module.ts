import { Module } from '@nestjs/common';
import { MeasurementController } from './measurement.controller';
import { MeasurementService } from './measurement.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementSchema } from './schemas/measurement.schema';
import { LocationService } from 'src/location/location.service';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'Measurement',
        schema: MeasurementSchema,
        collection: 'measurement',
      },
    ]),
  ],
  controllers: [MeasurementController],
  providers: [MeasurementService,
              //ViewsService
  ],
  exports: [MeasurementService],
})
export class MeasurementModule {}
