import { IsString, IsNotEmpty } from 'class-validator';

export class CreateSerialConnectionDTO {
  @IsString()
  dataLength: string;

  @IsString()
  parity: string;

  @IsString()
  stopBits: string;

  @IsString()
  baudRate: string;

  @IsString()
  port: string;
}
