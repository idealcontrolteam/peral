import { Schema } from 'mongoose';

export const SerialConnectionSchema = new Schema(
  {
    dataLength: String,
    parity: String,
    stopBits: String,
    baudRate: String,
    port: String,
  },
  { versionKey: false },
);
