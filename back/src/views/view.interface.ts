
import { Document } from 'mongoose';

export interface View1 extends Document {
    name: String,
    shortName: String,
    unity: Number,
    meditions:[{
        dateTime:Date,
        value:Number,
        active:Boolean
    }]
  }
  