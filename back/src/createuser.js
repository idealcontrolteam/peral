const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
  name: String,
  email: String,
  username: String,
  password: String,
  roleId: {
    type: Schema.Types.ObjectId,
    ref: 'Role',
    required: true,
  },
  companyId: {
    type: Schema.Types.ObjectId,
    ref: 'Company',
    required: true,
  },
  active: Boolean,
  lastConnection: {
    type: Date,
    default: null,
  },
}, {
  versionKey: false
}, );




mongoose
  .connect("mongodb://localhost:27017/bdf4f",{ useNewUrlParser: true }) 
      .then(mongodb => {    
        let hashedPassword = bcrypt.hash('aquagen', 10);
      
        return mongodb.model('user', UserSchema, 'user').create({
          name: 'Usuario Aquagen',
          email: 'aquagen@aaquagen.cl',
          username: 'Administrator',
           password: hashedPassword,
          roleId: "5ccaf9ba3791fe65dce19dea",
          companyId: "5ccaf9ba3791fe65dce19deb",
          active: true,
          lastConnection: Date.now()
        });
      })
      .then(newUser => {
        console.log('[NEW USER CREATED]', newUser.name);
        console.log('Username: ' + newUser.email);
        console.log('Password: 123456');
      })
      .catch(err => {
        console.log(err);
      })


  .catch(err => {
    console.log(err);
  });