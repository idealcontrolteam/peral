import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateConfigControlDTO } from './dto/TagConfigControl.dto';
import { TagConfigControl } from './interfaces/TagConfigControl.interface';

@Injectable()
export class TagConfigControlService {
  constructor(
    @InjectModel('TagConfigControl')
    private tagConfigControlModel: Model<TagConfigControl>,
  ) {}

  async getTagConfigControls(): Promise<TagConfigControl[]> {
    const tagConfigControls = await this.tagConfigControlModel.find();
    return tagConfigControls;
  }

  async getTagConfigControlsAll(): Promise<TagConfigControl[]> {
    const tagConfigControls = await this.tagConfigControlModel
      .find()
      .populate('tagId');
    return tagConfigControls;
  }

  async geTagConfigControlsByTagId(tagId): Promise<TagConfigControl[]> {
    const tagConfigControls = await this.tagConfigControlModel.find({
      tagId: tagId,
    });
    return tagConfigControls;
  }

  async getTagConfigControl(id): Promise<TagConfigControl> {
    const tagConfigControl = await this.tagConfigControlModel.findById(id);
    return tagConfigControl;
  }

  async getTagConfigControlForTag(id): Promise<any> {
    const tagConfigControl = await this.tagConfigControlModel.find({"tagId":id});
    return tagConfigControl;
  }

  async createTagConfigControl(
    createConfigControlDTO: CreateConfigControlDTO,
  ): Promise<TagConfigControl> {
    const newTagConfigControls = new this.tagConfigControlModel(
      createConfigControlDTO,
    );
    return await newTagConfigControls.save();
  }

  async deleteTagConfigControl(id): Promise<TagConfigControl> {
    return await this.tagConfigControlModel.findByIdAndDelete(id);
  }

  async updateTagConfigControl(
    id: string,
    body: CreateConfigControlDTO,
  ): Promise<TagConfigControl> {
    return await this.tagConfigControlModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
