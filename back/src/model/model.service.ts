import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as ModelI from './interfaces/model.interface';
import { CreateModelDTO } from './dto/model.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';

@Injectable()
export class ModelService {
  constructor(
    @InjectModel('Model') private modelModel: Model<ModelI.Model>,
    private deviceService: DeviceService,
  ) {}

  async getModels(): Promise<ModelI.Model[]> {
    const models = await this.modelModel.find();
    return models;
  }

  async getModel(id): Promise<ModelI.Model> {
    const model = await this.modelModel.findById(id);
    return model;
  }

  async getModelsAll(): Promise<ModelI.Model[]> {
    /*this.modelModel.schema.virtual('brand', {
      ref: 'Brand',
      localField: 'brandId',
      foreignField: '_id',
      justOne: true,
    });

    this.modelModel.schema.set('toObject', { virtuals: true });
    this.modelModel.schema.set('toJSON', { virtuals: true });*/

    const models = await this.modelModel.find().populate('brandId');

    /*this.modelModel.schema.set('toObject', { virtuals: false });
    this.modelModel.schema.set('toJSON', { virtuals: false });*/
    return models;
  }

  async getModelsByBrandId(brandId): Promise<ModelI.Model[]> {
    const models = await this.modelModel.find({ brandId: brandId });
    return models;
  }

  async getDevicesByModelId(modelid): Promise<Device[]> {
    const devices = await this.deviceService.getDevicesByModelId(modelid);
    return devices;
  }

  async createModel(createModelDTO: CreateModelDTO): Promise<ModelI.Model> {
    const newModel = new this.modelModel(createModelDTO);
    return await newModel.save();
  }

  async deleteModel(id): Promise<ModelI.Model> {
    const deletedDevices = await this.deviceService.deleteDevicesByModelId(id);
    console.log('[DELETED DEVICES]', deletedDevices);
    return await this.modelModel.findByIdAndDelete(id);
  }

  async deleteModelsByBrandId(brandId) {
    return await this.modelModel.deleteMany({ brandId: brandId });
  }

  async updateModel(id: string, body: CreateModelDTO): Promise<ModelI.Model> {
    return await this.modelModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
