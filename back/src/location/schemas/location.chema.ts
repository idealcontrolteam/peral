import { Schema } from 'mongoose';

export const LocationSchema = new Schema(
  {
    code: String,
    name: String,
    active: Boolean,
    zoneId: {
      type: Schema.Types.ObjectId,
      ref: 'Zone',
      required: true,
    },
    order: Number,
  },
  { versionKey: false },
);
