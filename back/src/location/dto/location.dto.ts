export class CreateLcationDTO {
  code: string;
  name: string;
  active: boolean;
  zoneId: string;
  order: number;
}
