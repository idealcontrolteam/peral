import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateDeviceDTO } from './dto/device.dto';
import { DeviceService } from './device.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('device')
//@UseGuards(new AuthGuard())
export class DeviceController {
  constructor(private deviceService: DeviceService) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'name' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post()
  async createDevice(@Res() res, @Body() body: CreateDeviceDTO) {
    this.validateIds(body);
    const newDevice = await this.deviceService.createDevice(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Device created successfully',
      data: newDevice,
    });
  }

  @Get()
  async getDevices(@Res() res) {
    const devices = await this.deviceService.getDevices();

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/all')
  async getDevicesAll(@Res() res) {
    const devices = await this.deviceService.getDevicesAll();

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/:deviceid/tag')
  async getTagsByDeviceId(@Res() res, @Param('deviceid') deviceid) {
    if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Device id is not a valid ObjectId');
    }

    const tags = await this.deviceService.getTagsByDeviceId(deviceid);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:deviceid')
  async getDevice(@Res() res, @Param('deviceid') deviceid) {
    if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Device id is not a valid ObjectId');
    }

    const device = await this.deviceService.getDevice(deviceid);
    if (!device) {
      throw new NotFoundException('Device not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Device found',
      data: device,
    });
  }

  @Put('/:deviceid')
  async updateDevice(
    @Res() res,
    @Body() body: CreateDeviceDTO,
    @Param('deviceid') deviceid,
  ) {
    if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Device id is not a valid ObjectId');
    }
    this.validateIds(body);

    const updatedDevice = await this.deviceService.updateDevice(deviceid, body);
    if (!updatedDevice) {
      throw new NotFoundException('Device not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Device updated',
      data: updatedDevice,
    });
  }

  @Delete('/:deviceid')
  async deleteDevice(@Res() res, @Param('deviceid') deviceid) {
    if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Device id is not a valid ObjectId');
    }
    const deletedDevice = await this.deviceService.deleteDevice(deviceid);

    if (!deletedDevice) {
      throw new NotFoundException('Device not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Device deleted',
      data: deletedDevice,
    });
  }
}
