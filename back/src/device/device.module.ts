import { Module } from '@nestjs/common';
import { DeviceController } from './device.controller';
import { DeviceService } from './device.service';
import { MongooseModule } from '@nestjs/mongoose';
import { DeviceSchema } from './schemas/device.schema';
import { TagModule } from '../tag/tag.module';
import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    TagModule,
    MongooseModule.forFeature([
      { name: 'Device', schema: DeviceSchema, collection: 'device' },
    ]),
  ],
  controllers: [DeviceController],
  providers: [DeviceService, TagService],
  exports: [DeviceService],
})
export class DeviceModule {}
