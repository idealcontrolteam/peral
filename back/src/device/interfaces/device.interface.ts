import { Document } from 'mongoose';

export interface Device extends Document {
  code: string;
  name: string;
  active: boolean;
  modelId: string;
  tcpConnectionId?: string;
  serialConnectionId?: string;
  gatewayId: string;
}
