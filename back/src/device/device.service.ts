import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Device } from './interfaces/device.interface';
import { CreateDeviceDTO } from './dto/device.dto';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';

@Injectable()
export class DeviceService {
  constructor(
    @InjectModel('Device') private deviceModel: Model<Device>,
    private tagService: TagService,
  ) {}

  async getDevices(): Promise<Device[]> {
    const devices = await this.deviceModel.find();
    return devices;
  }

  async getDevice(id): Promise<Device> {
    const device = await this.deviceModel.findById(id);
    return device;
  }

  async getDevicesAll(): Promise<Device[]> {
    const devices = await this.deviceModel
      .find()
      .populate('modelId tcpConnectionId serialConnectionId gatewayId');
    return devices;
  }

  async getDevicesByModelId(modelId): Promise<Device[]> {
    const devices = await this.deviceModel.find({ modelId: modelId });
    return devices;
  }

  async getTagsByDeviceId(deviceId): Promise<Tag[]> {
    console.log('[IN DEVICE SERVICE]', deviceId);
    const tags = await this.tagService.getTagsByDeviceId(deviceId);
    return tags;
  }

  async getDevicesByTcpConnectionId(tcpConnectionId): Promise<Device[]> {
    const devices = await this.deviceModel.find({
      tcpConnectionId: tcpConnectionId,
    });
    return devices;
  }

  async getDevicesBySerialConnectionId(serialConnectionId): Promise<Device[]> {
    const devices = await this.deviceModel.find({
      serialConnectionId: serialConnectionId,
    });
    return devices;
  }

  async getDevicesByGatewayId(gatewayId): Promise<Device[]> {
    const devices = await this.deviceModel.find({
      gatewayId: gatewayId,
    });
    return devices;
  }

  async createDevice(createDeviceDTO: CreateDeviceDTO): Promise<Device> {
    const newDevice = new this.deviceModel(createDeviceDTO);
    return await newDevice.save();
  }

  async deleteDevice(id): Promise<Device> {
    return await this.deviceModel.findByIdAndDelete(id);
  }

  async deleteDevicesByModelId(modelId) {
    return await this.deviceModel.deleteMany({ modelId: modelId });
  }

  async updateDevice(id: string, body: CreateDeviceDTO): Promise<Device> {
    return await this.deviceModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
