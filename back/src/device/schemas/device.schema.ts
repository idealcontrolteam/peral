import { Schema } from 'mongoose';

export const DeviceSchema = new Schema(
  {
    code: String,
    name: String,
    active: Boolean,
    modelId: {
      type: Schema.Types.ObjectId,
      ref: 'Model',
      required: true,
    },
    tcpConnectionId: {
      type: Schema.Types.ObjectId,
      ref: 'TcpConnection',
      required: false,
    },
    serialConnectionId: {
      type: Schema.Types.ObjectId,
      ref: 'SerialConnection',
      required: false,
    },
    gatewayId: {
      type: Schema.Types.ObjectId,
      ref: 'Gateway',
      required: true,
    },
  },
  { versionKey: false },
);
