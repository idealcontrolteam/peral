import { IsString, IsBoolean, IsOptional } from 'class-validator';

export class CreateDeviceDTO {
  code: string;

  @IsString()
  name: string;

  @IsBoolean()
  active: boolean;

  @IsString()
  modelId: string;

  @IsString()
  @IsOptional()
  tcpConnectionId?: string;

  @IsString()
  @IsOptional()
  serialConnectionId?: string;

  @IsString()
  gatewayId: string;
}
