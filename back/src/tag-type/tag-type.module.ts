import { Module } from '@nestjs/common';
import { TagTypeController } from './tag-type.controller';
import { TagTypeService } from './tag-type.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TagTypeSchema } from './schemas/tagType.schema';
import { TagModule } from '../tag/tag.module';
import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    TagModule,
    MongooseModule.forFeature([
      { name: 'TagType', schema: TagTypeSchema, collection: 'tagType' },
    ]),
  ],
  controllers: [TagTypeController],
  providers: [TagTypeService, TagService],
})
export class TagTypeModule {}
