import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Brand } from './interfaces/brand.interface';
import * as ModelI from '../model/interfaces/model.interface';
import { CreateBrandDTO } from './dto/brand.dto';
import { ModelService } from '../model/model.service';

@Injectable()
export class BrandService {
  constructor(
    @InjectModel('Brand') private brandModel: Model<Brand>,
    private modelService: ModelService,
  ) {}

  async getBrands(): Promise<Brand[]> {
    const brands = await this.brandModel.find();
    return brands;
  }

  async getBrand(id): Promise<Brand> {
    const brand = await this.brandModel.findById(id);
    return brand;
  }

  async getModelsByBrandId(brandId): Promise<ModelI.Model[]> {
    return await this.modelService.getModelsByBrandId(brandId);
  }

  async createBrand(createBrandDTO: CreateBrandDTO): Promise<Brand> {
    const newBrand = new this.brandModel(createBrandDTO);
    return await newBrand.save();
  }

  async deleteBrand(id): Promise<Brand> {
    //return await this.brandModel.findModels(id);
    const result = await this.modelService.deleteModelsByBrandId(id);
    console.log(result);
    return await this.brandModel.findByIdAndDelete(id);
  }

  async updateBrand(
    id: string,
    createBrandDTO: CreateBrandDTO,
  ): Promise<Brand> {
    //return the object updated
    return await this.brandModel.findByIdAndUpdate(id, createBrandDTO, {
      new: true,
    });
  }
}
