import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { WorkPlaceService } from './work-place.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('workPlace')
//@UseGuards(new AuthGuard())
export class WorkPlaceController {
  constructor(private workPlaceService: WorkPlaceService) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key == 'companyId' || key == 'categoryId') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post()
  async createWorkPlace(@Res() res, @Body() body: CreateWorkPlaceDTO) {
    this.validateIds(body);
    const newWorkPlace = await this.workPlaceService.createWorkPlace(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Work place created successfully',
      data: newWorkPlace,
    });
  }

  @Get()
  async getWorkPlaces(@Res() res) {
    const workPlaces = await this.workPlaceService.getWorkPlaces();

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Get('/all')
  async getWorkPlacesAll(@Res() res) {
    const workPlaces = await this.workPlaceService.getWorkPlacesAll();

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Get('/:idWorkPlace/gateway')
  async getGateWaysByWorkPlaceId(
    @Res() res,
    @Param('idWorkPlace') idWorkPlace,
  ) {
    const gateways = await this.workPlaceService.getGateWaysByWorkPlaceId(
      idWorkPlace,
    );

    let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: gateways,
      count: gateways.length,
    });
  }

  @Get('/:idWorkPlace/zone')
  async getZonesByWorkPlaceId(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    const zones = await this.workPlaceService.getZonesByWorkPlaceId(
      idWorkPlace,
    );

    let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: zones,
      count: zones.length,
    });
  }

  @Get('/:idWorkPlace')
  async getWorkPlace(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }

    const workPlace = await this.workPlaceService.getWorkPlace(idWorkPlace);
    if (!workPlace) {
      throw new NotFoundException('Work place not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place found',
      data: workPlace,
    });
  }

  @Put('/:idWorkPlace')
  async updateWorkPlace(
    @Res() res,
    @Body() body: CreateWorkPlaceDTO,
    @Param('idWorkPlace') idWorkPlace,
  ) {
    if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Work place id is not a valid ObjectId');
    }
    this.validateIds(body);

    const updatedWorkPlace = await this.workPlaceService.updateWorkPlace(
      idWorkPlace,
      body,
    );
    if (!updatedWorkPlace) {
      throw new NotFoundException('Work place not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place updated',
      data: updatedWorkPlace,
    });
  }

  @Delete('/:idWorkPlace')
  async deleteWorkPlace(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    const deletedWorkPlace = await this.workPlaceService.deleteWorkPlace(
      idWorkPlace,
    );

    if (!deletedWorkPlace) {
      throw new NotFoundException('Work place not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place deleted',
      data: deletedWorkPlace,
    });
  }
}
