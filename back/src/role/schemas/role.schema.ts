import { Schema } from 'mongoose';

export const RoleSchema = new Schema(
  {
    name: String,
    active: Boolean,
  },
  { versionKey: false },
);
