import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateRoleDTO } from './dto/role.dto';
import { RoleService } from './role.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('role')
//@UseGuards(new AuthGuard())
export class RoleController {
  constructor(private roleService: RoleService) {}

  @Post()
  async createRole(@Res() res, @Body() body: CreateRoleDTO) {
    const newRole = await this.roleService.createRole(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Role created successfully',
      data: newRole,
    });
  }

  @Get()
  async getRoles(@Res() res) {
    const roles = await this.roleService.getRoles();

    let msg = roles.length == 0 ? 'Roles not found' : 'Roles fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: roles,
      count: roles.length,
    });
  }

  @Get('/:roleId/user')
  async getUsersByRoleId(@Res() res, @Param('roleId') roleId) {
    const users = await this.roleService.getUsersByRoleId(roleId);

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/:roleId')
  async getRole(@Res() res, @Param('roleId') roleId) {
    if (!roleId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Id is not a valid  ObjectId');
    }

    const role = await this.roleService.getRole(roleId);
    if (!role) {
      throw new NotFoundException('Role not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Role found',
      data: role,
    });
  }

  @Put('/:roleId')
  async updateRole(
    @Res() res,
    @Body() body: CreateRoleDTO,
    @Param('roleId') roleId,
  ) {
    if (!roleId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Id is not a valid  ObjectId');
    }
    const updatedRole = await this.roleService.updateRole(roleId, body);
    if (!updatedRole) {
      throw new NotFoundException('Role not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Role updated',
      data: updatedRole,
    });
  }

  @Delete('/:roleId')
  async deleteRole(@Res() res, @Param('roleId') roleId) {
    const deletedRole = await this.roleService.deleteRole(roleId);

    if (!deletedRole) {
      throw new NotFoundException('Role not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Role deleted',
      data: deletedRole,
    });
  }
}
