import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFailDTO } from './dto/fail.dto';
import { Fail } from './interfaces/fail.interface';

@Injectable()
export class FailService {
  constructor(@InjectModel('Fail') private failModel: Model<Fail>) {}

  async getFails(): Promise<Fail[]> {
    const fails = await this.failModel.find();
    return fails;
  }

  async getFailsAll(): Promise<Fail[]> {
    const fails = await this.failModel.find().populate('tagId');
    return fails;
  }

  async getFailsByTagId(tagId): Promise<Fail[]> {
    const fails = await this.failModel.find({ tagId: tagId });
    return fails;
  }

  async getFail(id): Promise<Fail> {
    const fail = await this.failModel.findById(id);
    return fail;
  }

  async createFail(createFailDTO: CreateFailDTO): Promise<Fail> {
    const newFail = new this.failModel(createFailDTO);
    return await newFail.save();
  }

  async deleteFail(id): Promise<Fail> {
    return await this.failModel.findByIdAndDelete(id);
  }

  async updateFail(id: string, body: CreateFailDTO): Promise<Fail> {
    return await this.failModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
  async getFailByfilteredDate(   
    fini,
    ffin,
  ): Promise<Fail[]> {
    const fails = await this.failModel.find({      
      dateTimeIni: { $gte: fini, $lte: ffin },
    });
    return fails;
  }
}
