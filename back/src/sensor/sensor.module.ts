import { Module } from '@nestjs/common';
import { SensorController } from './sensor.controller';
import { SensorService } from './sensor.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SensorSchema } from './schemas/sensor.schema';
import { MeasurementModule } from '../measurement/measurement.module';
import { MeasurementService } from '../measurement/measurement.service';
import { TagModule } from '../tag/tag.module';
import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    MeasurementModule,
    TagModule,
    MongooseModule.forFeature([
      { name: 'Sensor', schema: SensorSchema, collection: 'sensor' },
    ]),
  ],
  controllers: [SensorController],
  providers: [SensorService, MeasurementService, TagService],
  exports: [SensorService],
})
export class SensorModule {}
