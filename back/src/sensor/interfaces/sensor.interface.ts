import { Document } from 'mongoose';

export interface Sensor extends Document {
  name: string;
  serialNumber: string;
  active: boolean;
  locationId: string;
  shortName: string;
  description: string;
}
