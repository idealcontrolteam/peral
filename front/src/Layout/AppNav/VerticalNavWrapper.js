import React, {Component, Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import MetisMenu from 'react-metismenu';
// import {Menu } from './NavItems';
import logo from '../../assets/img/sasa.png';
import Media from 'react-media';
import {TagServices} from '../../services/comun'
//import {Menu,Salas } from './NavItems';

class Nav extends Component {
    constructor() {
        super();
        
        this.state = {  
            Modulos:[]
        }
        this.tagservices=new TagServices();
    }

    componentDidMount = () => {
        this.tagservices.getZone().then(response=>{
            //console.log(response)
            if(response.statusCode===200 || response.statusCode===200){
                let Modulos=[]
                response.data.map(z=>{
                    // let name=z.name.split(' ')
                    Modulos.push({
                        label: z.name,
                        to: `#/dashboards/modulo/${z.code}`,
                    })
                })
                this.setState({Modulos})
            }
        })
    }
    render() {
        let Menu = [
            {
                icon: 'pe-7s-display1',
                label: 'Panel General',
                to: '#/dashboards/panelgeneral',
            }, 
            {
                icon: 'pe-7s-display1',
                label: 'Tendencia Historica',
                to: '#/dashboards/tendencia',
            },
            {
                icon: 'pe-7s-display1',
                label: 'Modulos',
                content: this.state.Modulos,
            },
        ];
        return (
            <Fragment>
                <div className="app-sidebar__heading header-btn-lg">
        
                        <div className=" ml-3 ">
                                <div className="">
                                    Piscicultura
                                </div>
                                <div className="opacity-4">
                                    Peral
                                </div>
                         </div>
                 
                </div>
                <h5 className="app-sidebar__heading">Menu</h5>
                <MetisMenu  content={Menu} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix=""  classNameStateIcon="pe-7s-angle-down"/>
                <Media query="(max-height: 725px)">
                    {matches =>
                        matches ? (
                            <img className="animated" src={logo} width="230" style={{marginTop:270,borderRadius:4}} />
                        ) : (
                            <img className="animated" src={logo} width="230" style={{marginTop:400,borderRadius:4,float:"left"}} />
                        )
                    }
                </Media>
                {/* <h5 className="app-sidebar__heading">Salas</h5>
                <MetisMenu  content={Salas} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix="" classNameStateIcon="pe-7s-angle-down"/> */}
            </Fragment>
        );
    }

    isPathActive(path) {
        return this.props.location.pathname.startsWith(path);
    }
}

export default withRouter(Nav);