export const Menu = [
    {
        icon: 'pe-7s-display1',
        label: 'Panel General',
        to: '#/dashboards/panelgeneral',
    }, 
    {
        icon: 'pe-7s-display1',
        label: 'Tendencia Historica',
        to: '#/dashboards/tendencia',
    },
    {
        icon: 'pe-7s-display1',
        label: 'Modulos',
        content: [
            {
                label: 'Modulo 100',
                to: '#/dashboards/modulo/1',
            },
            {
                label: 'Modulo 200',
                to: '#/dashboards/modulo/2',
            },
            {
                label: 'Modulo 400',
                to: '#/dashboards/modulo/4',
            },
            {
                label: 'Modulo 500',
                to: '#/dashboards/modulo/5',
            },
        ],
    },
];

// export const Salas = [
 
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Broodstock',
//         to: '#/dashboards/zonas1',
//     },
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Smolt',
//         to: '#/dashboards/zonas2',
//     }
//     ,
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Alevines',
//         to: '#/dashboards/zonas3',
//     }
 
 
// ];


