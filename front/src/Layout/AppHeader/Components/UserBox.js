import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';
import Ionicon from 'react-ionicons';
import {logout} from '../../../services/user';
//import {logout, currentAccount} from '../../../services/user';
import {connect} from 'react-redux';
// import CryptoJS from 'crypto-js';


import {

    Button
    
} from 'reactstrap';

import {
    toast,
    Bounce
} from 'react-toastify';


class UserBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
        };
        
      
    }
   
   
   
    notify2 = () => this.toastId = toast("You don't have any new items in your calendar for today! Go out and play!", {
        transition: Bounce,
        closeButton: true,
        autoClose: 5000,
        position: 'bottom-center',
        type: 'success'
    });


    handleLogout = () => {
        logout();
        this.setState({ active: true });  
 
    }
    

    render() {  
         let {nameUsuario,emailUsuario } = this.props;       
   
        if (this.state.active) {
            return <Redirect to="/pages/login"></Redirect>
        }   
        return (
            <Fragment>

                <div className="header-btn-lg pr-0">
                    <div className="widget-content p-0">
                        <div className="widget-content-wrapper">
                            <div className="widget-content-left  "> 
                                 <div className="header-user-icon">
                                    <i  className="rounded-circle pe-7s-user pe-3x  "/>
                                </div>  
                            </div>

                            <div className="widget-content-left  ml-3 ">
                                <div className="widget-heading">
                                    {nameUsuario}
                                </div>
                                <div className="widget-subheading">
                                   {emailUsuario}
                                </div>
                            </div>

                            <div className="widget-content-right  ml-3">
                                <Button onClick={this.handleLogout}  className="btn-shadow p-1" size="sm"  color="primary">
                                    <Ionicon color="#ffffff" fontSize="20px" icon="ios-exit"/>
                                </Button>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}




const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,

});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UserBox);
