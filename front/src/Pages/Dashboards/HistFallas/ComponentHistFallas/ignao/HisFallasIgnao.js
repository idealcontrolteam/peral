import React, {Component, Fragment} from 'react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,Button,CardHeader} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import ReactTable from "react-table";

import { CSVLink } from "react-csv";


import { API_ROOT } from '../../../../../api-config';
import axios from 'axios';

const API1= `${API_ROOT}/fail/2019-03-27/2019-12-01`;


export default class HistFallasIgnao extends Component {
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
        start = moment(start).subtract(34, "months").subtract(1, "seconds");
        end = moment(start).add(5, "days").add();
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          HistFail:[],
          PresentFail:[] 
         
         
        };     
       
        this.applyCallback = this.applyCallback.bind(this);
        this.filtrarFallasPresentes = this.filtrarFallasPresentes.bind(this);
        this.filtrarFallasHistoricas = this.filtrarFallasHistoricas.bind(this);        
        this.renderPickerAutoApply = this.renderPickerAutoApply.bind(this);

  

      
      
    
    }

    componentDidMount = () => {
        this.filtrarFallasPresentes();
      }


    filtrarFallasHistoricas = () =>{
            //const token = localStorage.getItem("token");        
            const token = "tokenfalso";
    
            axios
            .get(API1, {
              headers: {  
                'Authorization': 'Bearer ' + token
              }
            })
            .then(response => {
                const myData= response.data.data; 
               
                const HistFail = myData.filter((data) => {                        
                    return data.dateTimeTer !== undefined
                })
                .map( item => { 
                 return { 
                     description: item.description,
                     dateTimeIni: moment(item.dateTimeIni.substr(0,19)).format('DD-MM-YYYY HH:mm') , 
                    dateTimeTer : moment(item.dateTimeTer.substr(0,19)).format('DD-MM-YYYY HH:mm'),
                     presentValue : item.presentValue,
                     failValue: item.failValue
                    
                    }; 
                 });

           
          this.setState({HistFail: HistFail });   
        
            })
            .catch(error => {
              console.log(error);
            });
       
    }  
    
    filtrarFallasPresentes = () =>{
        //const token = localStorage.getItem("token");        
        const token = "tokenfalso";

        axios
        .get(API1, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myData= response.data.data;    
            const PresentFail = myData.filter((data) => {                             
                   return data.dateTimeTer === undefined
            }).map( item => { 
                return { 
                    description: item.description,
                    dateTimeIni: moment(item.dateTimeIni.substr(0,19)).format('DD-MM-YYYY HH:mm'),                  
                    presentValue : item.presentValue,
                    failValue: item.failValue
                   
                   }; 
                });
 
      this.setState({PresentFail: PresentFail });   
        })
        .catch(error => {
          console.log(error);
        });
   
   }  
    applyCallback(startDate, endDate){      
        this.setState({
            start: startDate,
            end: endDate
        });
    }



    renderPickerAutoApply = (ranges, local, maxDate)  => {  
        let value1 = this.state.start.format("DD-MM-YYYY HH:mm") 
        let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
       }

    render() {
        
    //     ///***********************************+++ */
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
        let end = moment(start).add(1, "days").subtract(1, "seconds");         
        let ranges = {
        "Solo hoy": [moment(start), moment(end)],
        "Solo ayer": [
          moment(start).subtract(1, "days"),
          moment(end).subtract(1, "days")
        ],
        "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
        "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
        "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
        "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
        "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
        "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
        "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
      };
      let local = {
        format: "DD-MM-YYYY HH:mm",
        sundayFirst: false
      };
      let maxDate = moment(start).add(24, "hour");
      ///***********************************+++ */
      
        return (
            <Fragment>
        
                        <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                       FALLAS
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Row>                       
                            <Col md="12">
                                <Card className="main-card mb-1 p-0">
                                    <CardBody className="p-3">                                 
                                        <Row>
                                            <Col   md={12} lg={3}> 
                                                {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                            </Col>
                                            <Col md={12}  lg={8}>
                                          
                                            </Col>
                                            <Col   md={12} lg={1}> 
                                         
                                            </Col>                              



                                        </Row>
                                    </CardBody>
                                </Card>

                            </Col>
                    
                    </Row>

                        <Row>   


                                   
                                  <Col md="6"   >
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     HISTORICO DE FALLAS
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                <Button color="primary"
                                                   outline
                                                   className={"btn-shadow btn-wide btn-outline-2x pb-2 mr-2 "}
                                   
                                                        onClick={() => { this.filtrarFallasHistoricas(); }}
                                                >Filtrar
                                              
                                                </Button>
                                                      
                                                        <CSVLink
                                                            
                                                            separator={";"}                                                                   
                                                            headers={
                                                                [
                                                                    { label: 'ALARMA', key: "description" },
                                                                    { label: "INICIO", key: "dateTimeIni" },
                                                                    { label: 'TERMINO', key: "dateTimeTer" },
                                                                    { label: "OBTENIDO", key: "presentValue" } ,
                                                                    { label: "CONFIGURADO", key: "failValue" }
                                                                ]
                                                            
                                                            }
                                                            data={this.state.HistFail}
                                                            filename={"FallasHistorico.csv"}                                                                
                                                            target="_blank">
                                                            <Button color="primary"
                                                            outline
                                                            className={"btn-shadow btn-wide btn-outline-2x pb-2  "}                                                 
                                                            > Exportar Datos</Button>
                                                        </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                         
                                                        <ReactTable                                                           
                                                            data={this.state.HistFail}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={5}
                                                            columns={[
                                                                {
                                                                    Header: "ALARMAS",
                                                                    accessor: "description",
                                                                    width: 300
                                                                    },
                                                                    {
                                                                    Header: "INICIO",
                                                                    accessor: "dateTimeIni",                                                              
                                                                    width: 130
                                                                    },
                                                                    {
                                                                    Header: "TERMINO",
                                                                    accessor: "dateTimeTer",                                                              
                                                                    width: 130
                                                                    },
                                                                    {
                                                                    Header: "OBTENIDO",
                                                                    accessor: "presentValue",                                                              
                                                                    width: 90
                                                                    }
                                                                    ,
                                                                    {
                                                                    Header: "CONFIGURADO",
                                                                    accessor: "failValue",                                                              
                                                                    width: 90
                                                                    }
                                                                ]                                                             
                                                            }

                                                  
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>


                                        <Col md="6"   >
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                      FALLAS PRESENTES
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                <Button color="primary"
                                                   outline
                                                   className={"btn-shadow btn-wide btn-outline-2x pb-2 mr-2 "}
                                   
                                                        onClick={() => { this.filtrarFallasPresentes(); }}
                                                >Actualizar
                                              
                                                </Button>
                                                      
                                                        <CSVLink
                                                                        
                                                            separator={";"}                                                                   
                                                            headers={
                                                                [
                                                        
                                                                    { label: 'ALARMA', key: "description" },
                                                                    { label: "INICIO", key: "dateTimeIni" },                                                                
                                                                    { label: "OBTENIDO", key: "presentValue" } ,
                                                                    { label: "CONFIGURADO", key: "failValue" }
                                                                ]
                                                            
                                                            }
                                                            data={this.state.PresentFail}
                                                            filename={"FallasPresentes.csv"}                                                                      
                                                            target="_blank">
                                                            <Button color="primary"
                                                            outline
                                                            className={"btn-shadow btn-wide btn-outline-2x pb-2  "}                                                 
                                                            > Exportar Datos</Button>
                                                        </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                           
                                                        <ReactTable                                                           
                                                            data={this.state.PresentFail}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={5}
                                                            columns={[
                                                                {
                                                                    Header: "ALARMAS",
                                                                    accessor: "description",
                                                                    width: 300
                                                                    },
                                                                    {
                                                                    Header: "INICIO",
                                                                    accessor: "dateTimeIni",                                                              
                                                                    width: 130
                                                                    },                                                                  
                                                                    {
                                                                    Header: "OBTENIDO",
                                                                    accessor: "presentValue",                                                              
                                                                    width: 90
                                                                    }
                                                                    ,
                                                                    {
                                                                    Header: "CONFIGURADO",
                                                                    accessor: "failValue",                                                              
                                                                    width: 90
                                                                    }
                                                                ]                                                             
                                                            }

                                                  
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                        
                    </Row>

                   
                    
            </Fragment>
        );
    }
}