import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';
import {
    toast

} from 'react-toastify';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    Badge,ButtonGroup,Breadcrumb, BreadcrumbItem
} from 'reactstrap';
import Media from 'react-media';
//CustomInput,
import '../../../../assets/style.css'

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';

import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';
import { SizeMe } from 'react-sizeme';
import { number } from 'prop-types';
import {TagServices} from '../../../../services/comun'
import _ from 'lodash'

let TK101 = "6047782669e89545ec0047e1";

let sw=false;
let c_sw=false;
let c_sw2=false;
let c_sw3=false;
let visible="block";

let check=false;
let modificacion_general=false;
let habilitado =false;
let array_gloabal=[];
let array_tags_global=[];
//localStorage.setItem("check1", true);
//localStorage.removeItem("check1");



export default class IndexZone extends Component {   
    constructor() {
        super();
        
        this.state = {   
            myDataChart1: [],   
            isLoading: false,
            error: null,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
            
            bitValTK101:0,
            bitVenTK101:0,
            // bitValTK102:0,
            // bitVenTK102:0,
            // bitValS3:0,
            // bitVenS3:0,
            mySalaModal:0,
            Tags:[],
            nCard:[1],
            check:true,
            locations:[],
            Modulos:[],
            Data:[],
            Mediciones:[],
            id_setpointOx:1,
            id_minOx:3,
            id_maxOx:0,
            id_timeOn:5,
            id_timeOff:0,
            id_histeresisOx:0,
            id_ConfigTag:0,
            id_estadoTK:0,
            zoneId:"",
            locationId:"",
        }
        this.toggleModal1 = this.toggleModal1.bind(this);
        this.almacenarConfig = this.almacenarConfig.bind(this);
        this.confirmacionTermino = this.confirmacionTermino.bind(this);
        this.timerRef=React.createRef();
        this.tagservices=new TagServices();
    }
    
  
    componentDidMount = () => {
     //   window.location.reload();
        const {n_modulo}=this.props;
        // console.log(n_modulo)
        check=false;
        this.setState({ isLoading: true });
        this.loadAllData(n_modulo);
        // const intervaloRefresco =240000;
        // this.intervalIdtag = setInterval(() => this.loadDataTags(),intervaloRefresco);
        const hostname = window && window.location && window.location.hostname;
        if(hostname === 'ihc.idealcontrol.cl'){
            habilitado = true;
        }
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        // console.log(nextProps.n_modulo)
        array_gloabal=[];
        array_tags_global=[];
        this.loadAllData(nextProps.n_modulo);
      }

    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
       clearInterval(this.intervalConfirmacion);
       clearInterval(this.intervalIdtag);
       clearInterval(this.timerRef);
       clearInterval(this.loadAllData);
    }  

    loadAllData = (n_modulo) =>  {  
        this.tagservices.getZoneModulo(n_modulo).then(zone=>{
            // console.log(zone[0]._id)
            this.tagservices.getLocationsZone(zone[0]._id).then(locations=>{
                let array=locations.map((l)=>{ return{"locationId":l._id} })
                // console.log(locations)
                array_gloabal=array
                this.loadDataTags()
                this.setState({Data:locations,Modulos:locations.map(l=>l.zoneId.name)})
            })
        })
    }

    loadDataTags(){
        this.tagservices.getTagsArray(array_gloabal).then(tags=>{
            let array=tags
            .filter(t=>t.shortName.toLowerCase()=='oxd'||
                    t.shortName.toLowerCase()==('oxs')||
                    t.shortName.toLowerCase()==('temp')||
                    t.shortName.toLowerCase()==('sal'))
            .map(t=>{return{"tagId":t._id}})

            array_tags_global=array
            this.getMediciones()
            const intervaloRefresco = 300000;
            this.timerRef.current = setInterval(this.getMediciones.bind(this),intervaloRefresco);
            if(tags.length>0){
                this.setState({Tags:tags,dateTimeRefresh:tags[0].dateTimeLastValue,isLoading: false})
            }else{
                this.setState({Tags:tags,isLoading: false})
            }
            
        })
    }

    getMediciones(){
        // array_tags_global
        this.tagservices.getRegistrosToday(array_tags_global).then(respuesta=>{
            this.setState({Mediciones:_.orderBy(respuesta,["dateTime"],["asc"])})
        })
    }

    toggleModal1(l) {
        if(l=="-1"){
            this.setState({
                modal1: !this.state.modal1,
            });
        }else{
            try{ 
                sw=this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase()=='oxd')[0].state;
                this.setState({
                    modal1: !this.state.modal1,
                    mySalaModal:l.name,
                    zoneId:l.zoneId,
                    locationId:l._id,
                    // setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('set_point'))[0].lastValue,
                    // maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('alarma_maxima'))[0].lastValue,
                    // minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('alarma_minima'))[0].lastValue,
                    // timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('time_on'))[0].lastValue,
                    // timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('time_off'))[0].lastValue,
                    // histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('histeresis'))[0].lastValue,
    
                    // id_setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('set_point'))[0]._id,
                    // id_maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('alarma_maxima'))[0]._id,
                    // id_minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('alarma_minima'))[0]._id,
                    // id_timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('time_on'))[0]._id,
                    // id_timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('time_off'))[0]._id,
                    // id_histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('histeresis'))[0]._id,
                    // id_ConfigTag:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('oxd'))[0]._id,

                    setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('set_point'))[0].lastValue,
                    maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('BANDA_MAX'))[0].lastValue,
                    minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('BANDA_MIN'))[0].lastValue,
                    timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_on'))[0].lastValue,
                    timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_off'))[0].lastValue,
                    histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('histeresis'))[0].lastValue,
                    estadoTK:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('HA_TK'))[0].lastValue,
    
                    id_setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('set_point'))[0]._id,
                    id_maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('BANDA_MAX'))[0]._id,
                    id_minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('BANDA_MIN'))[0]._id,
                    id_timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_on'))[0]._id,
                    id_timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_off'))[0]._id,
                    id_histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('histeresis'))[0]._id,
                    id_ConfigTag:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase()=='oxd')[0]._id,
                    id_estadoTK:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.includes('HA_TK'))[0]._id,
                    //botones de control
    
                });
            }catch(e){}
        }
    }


    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });   
        
        
        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            // console.log(response);
            // console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );      
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                this.componentDidMount();
                // this.loadDataTags();
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            }else if (this.state.intentosGuardado >= 10)
            {
                  //this.componentDidMount();
                  this.loadDataTags();
                  toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false}); 
                  clearInterval(this.intervalConfirmacion);
            }
        })
        .catch(error => {
          //console.log(error);
        });
    }



    almacenarConfig = () => {
        let data=[
            "setpointOx",
            "minOx",
            "maxOx",
            "timeOn",
            "timeOff",
            "histeresisOx",
            "estadoTK",
        ]
        let data2=[
            "SET_POINT",
            "BANDA_MAX_OXD",
            "BANDA_MIN_OXD",
            "TIME_ON",
            "TIME_OFF",
            "HISTERESIS",
            "HA_TK",
        ]
        let {id_ConfigTag} = this.state;
        let conf={
            "setpoint": this.state[data[0]],
            "histeresis": this.state[data[5]],
            "timeOn": this.state[data[3]],
            "timeOff": this.state[data[4]],
            "active":true,
        }
        if(modificacion_general){
            this.setState({blocking1:true})
            data.map((key,i)=>{
                let obj={
                    "lastValue":this.state[key],
                    "lastValueWrite":this.state[key],
                    "write":true
                }
                // this.state.Tags.filter(t=>t.zoneId==this.state.zoneId&&t.shortName.toLowerCase()=='oxd').map(t=>{
                //     this.tagservices.updateTagsConfig(t._id,conf).then(respuesta=>{
                //         //alert(JSON.stringify(respuesta))
                //     })
                // })
                this.state.Tags.filter(t=>t.zoneId==this.state.zoneId&&t.shortName==String(data2[i])).map(t=>{
                    this.tagservices.updateTags(t._id,obj).then(respuesta=>{
                        if(respuesta.statusCode==201 || respuesta.statusCode==200){
                            //toast['success']('Se ha actualizado el '+String(data2[i]), { autoClose: 4000 })
                            this.setState({
                                Tags:this.state.Tags.filter(t2=>t2._id!=t._id)
                                .concat(this.state.Tags.filter(t2=>t2._id==t._id)
                                .map(t2=>{
                                    t2.lastValue=obj.lastValue
                                    return t2
                                })),blocking1:false,
                            })
                        }
                    })
                })
                //Permite habilitar y deshabilitar modulo completo

                // let id_zone=this.state.zoneId
                // this.state.Tags.filter(l=>l.zoneId==id_zone).map(t=>{
                //     let obj={
                //         "state":sw,
                //     }
                //     this.tagservices.updateTags(t._id,obj).then(respuesta=>{
                //         if(respuesta.statusCode==201 || respuesta.statusCode==200){
                //             this.setState({
                //                 Tags:this.state.Tags.filter(t=>t.zoneId!=id_zone)
                //                 .concat(this.state.Tags.filter(t=>t.zoneId==id_zone)
                //                 .map(t=>{
                //                     t.state=sw
                //                     return t
                //                 }))
                //             })
                //         }
                //         //toast['success']('Se ha actualizado el '+tag.name, { autoClose: 4000 })
                //     })
                // })
            })
        }else{
            // alert("aqui")
            // this.tagservices.updateTagsConfig(id_ConfigTag,conf).then(respuesta=>{
            //     // alert(JSON.stringify(respuesta))
            // })
            data.map(key=>{
                let obj={
                    "lastValue":this.state[key],
                    "lastValueWrite":this.state[key],
                    "write":true
                }
                this.tagservices.updateTags(this.state["id_"+key],obj).then(respuesta=>{
                    if(respuesta.statusCode==201 || respuesta.statusCode==200){
                        toast['success']('Se ha actualizado el '+key, { autoClose: 4000 })
                        this.setState({
                            Tags:this.state.Tags.filter(t=>t._id!=this.state["id_"+key])
                            .concat(this.state.Tags.filter(t=>t._id==this.state["id_"+key])
                            .map(t=>{
                                t.lastValue=obj.lastValue
                                return t
                            }))
                        })
                    }
                })
            })
            // let id_location=this.state.locationId
            // this.state.Tags.filter(l=>l.locationId==id_location).map(t=>{
            //     let obj={
            //         "state":sw,
            //     }
            //     this.tagservices.updateTags(t._id,obj).then(respuesta=>{
            //         if(respuesta.statusCode==201 || respuesta.statusCode==200){
            //             this.setState({
            //                 Tags:this.state.Tags.filter(t=>t.locationId!=id_location)
            //                 .concat(this.state.Tags.filter(t=>t.locationId==id_location)
            //                 .map(t=>{
            //                     t.state=sw
            //                     return t
            //                 }))
            //             })
            //         }
            //         //toast['success']('Se ha actualizado el '+tag.name, { autoClose: 4000 })
            //     })
            // })
        }
    }
 
    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => { 
        // console.log(event.target.defaultChecked);  
        this.setState( { 
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }
    
    getColor(unidad){
        // #FC3939
        //1,,3,2
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            if(unidad=="bat"){
                return miscolores[10]
            }
            else if(unidad.toLowerCase()=="oxd"){
                return miscolores[0]
            }else if(unidad.toLowerCase()==("oxs")||unidad.includes("oxs")){
                return miscolores[2]
            }else if(unidad.toLowerCase()==("temp")){
                return miscolores[1]
            }else if(unidad.toLowerCase()==("sal")){
                return miscolores[3]
            }
            else{
                return ""
            }
    }
    //genera cada boton para el Oxs, Temp y Oxd
    getButton = (data,i,n)=>{
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        let mediciones=this.state.Mediciones.filter(m=>m.tagId==data._id)
        mediciones=mediciones.length>0?mediciones:[{"value":0}]
        return<div className="widget-chart widget-chart-hover  p-0 p-0 ">
             <Button 
                onClick={ e => {
                        try{
                            this["slider"+(n+1)].slickGoTo(i)
                        }catch(e){

                        }
                }}
                key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={this.getColor(data.shortName)}>  
                {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                <div className="size-boton mt-0  " style={{color:this.getColor(data.shortName)}} >                                                                                                                            
                {mediciones[mediciones.length-1].value}
                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                </div>
                <div className="widget-subheading">
                {data.name}
                </div>
            </Button>                                              
        </div>
    }
    
    //genera el grafico de Oxs, Temp y Oxd
    getChart = (tag,i)=>{
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if(this.state.check)
            return <div className="widget-chart widget-chart2 text-left p-0">
            <div className="widget-chat-wrapper-outer">
                <div className="widget-chart-content widget-chart-content-lg  p-2">
                    <div className="widget-chart-flex ">
                        <div
                            className="widget-title opacity-9 text-muted text-uppercase">
                            {tag.name}
                        </div>
                        
                        {/* <div className="btn-actions-pane-right text-capitalize pr-2" > 
                                <div className={cx("divfloatleft mr-2",this.state.bitVenTK101===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                    <img width={23} src={ventilador} alt="" />

                                </div>
                                <div className={cx("divfloatleft mr-2", this.state.bitValTK101===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                    <img width={23} src={valvula} alt="" />

                                </div>                                                                               
                        </div> */}
                    </div>

                    <div className="widget-numbers p-1 m-0">
                        <div className="widget-chart-flex">
                            <div>
                                {tag.mediciones[tag.mediciones.length-1].value}
                                <small className="opacity-5 pl-1 size_unidad3">  {tag.unity}</small>
                            </div>
                        </div>
                    </div> 
                    
                <div className=" opacity-8 text-focus pt-0">
                    <div className=" opacity-5 d-inline">
                    Max
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:this.getColor(tag.shortName)}}>  
                                                                                                    
                        <span className="pl-1 size_prom">
                                {                                             
                                tag.mediciones.reduce((max, b) => Math.max(max, b.value), tag.mediciones[0].value)                                             
                                }
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline  ml-2">
                    Prom
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:this.getColor(tag.shortName)}}>                                                                                  
                        <span className="pl-1 size_prom">
                            {
                                ( (tag.mediciones.reduce((a, b) => +a + +b.value, 0)/tag.mediciones.length)).toFixed(1)
                            }
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline ml-2">
                    Min
                    </div>
                    <div className="d-inline  pr-1" style={{color:this.getColor(tag.shortName)}}>                                                                                  
                        <span className="pl-1 size_prom">
                        {                                             
                            tag.mediciones.reduce((min, b) => Math.min(min, b.value), tag.mediciones[0].value)                                             
                            }
                        </span>
                    </div>
                    
                </div> 
            

                </div>

                {/* <div className="d-inline text-secondary pr-1">                                                                                                                                                                 
                        <span className="pl-1">
                                {data.dateTimeLastValue}

                        </span>
            </div> */}


                <div
                    className="widget-chart-wrapper he-auto opacity-10 m-0">
                    <ResponsiveContainer height={150} width='100%'>

                        <AreaChart data={tag.mediciones}
                                // animationDuration={1}
                                isAnimationActive = {false}
                                margin={{
                                    top: 0,
                                    right:10,
                                    left: -30,
                                    bottom: 0
                                }}>
                            
                                <Tooltip                                                                                                        
                                            labelFormatter={function(value) {
                                                return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                }}
                                            formatter={function(value, name) {
                                            return `${value}`;
                                            }}
                                        />
                                <defs>
                                    <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="10%" stopColor={this.getColor(tag.shortName)} stopOpacity={0.7}/>
                                        <stop offset="90%" stopColor={this.getColor(tag.shortName)}stopOpacity={0}/>
                                    </linearGradient>
                                </defs>
                                <YAxis                                                                                    
                                tick={{fontSize: '10px'}}
                                // domain={['dataMin - 4','dataMax + 4']}
                                />
                                <ReferenceLine y={
                                    this.state.Tags.filter(t=>t.locationId==tag.locationId&&t.shortName.toLowerCase('BANDA_MAX'))[0].lastValue
                                } label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                <ReferenceLine y={
                                    this.state.Tags.filter(t=>t.locationId==tag.locationId&&t.shortName.toLowerCase('BANDA_MIN'))[0].lastValue
                                }  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                <XAxis
                                        dataKey={'dateTime'}                                                                              
                                        hide = {false}
                                        tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                        tick={{fontSize: '10px'}}
                                        />
                                <Area type='monotoneX' dataKey='value'
                            
                                    stroke={this.getColor(tag.shortName)}
                                    strokeWidth='3'
                                    fillOpacity={1}
                                    fill={"url(#colorPv" + i + ")"}/>
                            </AreaChart>

                    </ResponsiveContainer>
                </div>
            </div>
        </div>
    }

    getActiveTag(locationId){
        let tags=this.state.Tags.filter(t=>t.locationId==locationId)
        //console.log(tags[0])
        return tags.length>0?tags[0].state:0
    }

    //realiza los filtro y la llamada a generar el button para los 3 tags de la card
    getViewButton=(array,n)=>{
        return array.filter(t=> t.shortName.toLowerCase()=='oxd'||
                                t.shortName.toLowerCase()==('oxs')||
                                t.shortName.toLowerCase()==('temp')||
                                t.shortName.toLowerCase()==('sal')||
                                t.shortName.toLowerCase()==('ph')).map((data,i)=>
           <Col xs="3" sm="3" md="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
               {this.getButton(data,i,n)}
           </Col>
       )     
    }

    //realiza los filtro y la llamada a generar los graficos para los 3 tags de la card
    getViewChart=(array)=>{
        
    }

    //realiza un switch por separado para optimizar la busqueda del Slider
    switchViewChart=(tags,i)=>{
        return tags.filter(t=>t.shortName.toLowerCase()=='oxd'||t.shortName.toLowerCase()==('oxs')||t.shortName.toLowerCase()==('temp')||t.shortName.toLowerCase()==('sal'))
        .map((t,i) =>{
            let mediciones=this.state.Mediciones.filter(m=>m.tagId==t._id)
            t.mediciones=mediciones
            try{
                return<div key={t._id}>
                    {this.getChart(t,i)}
                </div> 
            }catch(e){

            }
        }) 
    }

    getTagForId(typeData,id,typeId){
        let find_tag=[]
        find_tag=this.state.Tags.filter(t=>t[typeId]==id&&t.shortName=='HA_TK')
        // console.log(find_tag)
        if(find_tag.length>0){
            return typeData!=null?
            find_tag[0][typeData]:
            find_tag[0]
        }
    }

    getCard=(i,settings,location)=>{
        //style={{opacity:`${i==11 || i==10?0:10}`}}
      return  <Fragment><Card>
                            <CardHeader className="card-header-tab">
                                <div className="card-header-title font-size-ls text-capitalize font-weight-normal">
                                    <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                    {location.name}
                                </div>
                                <div className="btn-actions-pane-right text-capitalize">
                                    <span className="d-inline-block">
                                            <Button color="primary" onClick={() => this.toggleModal1(location)}
                                                outline>
                                                <i className="pe-7s-tools btn-icon-wrapper" ></i>
                                            </Button>
                                        
                                    </span>
                                </div>

                                </CardHeader>
                                <CardBody className="p-0" 
                                    style={{opacity:`${this.getTagForId("lastValue",location._id,"locationId")==1?"":"0.3"}`}} >
                                {this.getCardBody(i,settings,location)}
                                </CardBody>
                            </Card>
                            </Fragment>
    }

    getCardBody=(i,settings,location)=>{
        let slider={};
        slider= slider => (this["slider"+(i+1)]=slider)
        return <Fragment>
                                <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                    Valor actual de Sondas 
                                    </h6>
                                    <Card className="main-card mb-0">
                                        <div className="grid-menu grid-menu-4col">
                                            <Row className="no-gutters">
                                                {
                                                    this.getViewButton(this.state.Tags.filter(t=>t.locationId==location._id),i)
                                                }                                   
                                            </Row>
                                        </div>
                                    </Card>
                                    <div className="p-1 slick-slider-sm mx-auto" style={{visibility:`${this.state.check?"visible":"hidden"}`}}>

                                    <Slider ref={slider} {...settings}>
                                        {
                                            this.switchViewChart(this.state.Tags.filter(t=>t.locationId==location._id),i)
                                        }
                                    </Slider>                              
                                    
                                    </div>
                                </Fragment>
    }
    
    
    
    render() {
        var this_url=window.location.href;
        var n_nave=this_url.split('/');
        
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { myDataChart1,isLoading, error} = this.state;
         
         const settings = {
            autoplaySpeed:6000,
            autoplay: false,        
            centerMode: false,
            // infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       

        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

        let url=window.location;
        let n_modul=String(url).split('modulo/');
        
        return (
            <Fragment>     
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Modulos</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Modulo {this.props.n_modulo}</BreadcrumbItem>
                      
                      <Media queries={{ small: { maxWidth:771 },media:{minWidth:772,maxWidth: 1158} }}>
                        {matches =>(
                            <Fragment>
                            {matches.small &&
                                <ButtonGroup style={{marginLeft:`${80}%`,marginTop:-20}}>
                                <Button color="primary" outline={!this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline={this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            
                            {matches.media &&
                                <ButtonGroup style={{marginLeft:`${90}%`,marginTop:-20}}>
                                <Button color="primary" outline={!this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline={this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            {(!matches.media && !matches.small) &&
                                <ButtonGroup style={{marginLeft:`${92}%`,marginTop:-20}}>
                                <Button color="primary" outline={!this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline={this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            
                            </Fragment>
                        )}
                        </Media>
                            
                      </Breadcrumb>
                        <Row>
                            {
                                this.state.Data.map((c,i)=>{
                                    return<Col sm={6} md={6} key={i} style={{marginTop:15}} className="animated fadeIn fast">
                                        {this.getCard(i,settings,c)}
                                    </Col> 
                                })
                            }      

                        </Row>

                        <Modal isOpen={this.state.modal1}>
                                                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion {this.state.mySalaModal}</ModalHeader>
                                                    <ModalBody>

                                                    <Row>     
                                                    
                                                        <Col  xs="12" md="12" lg="12">
                                                            <Card className="main-card mb-3">
                                                                <CardBody>
                                                                    <CardTitle>Oxigeno</CardTitle>
                                                                    <Form>
                                                                        <Row>

                                                                            <Col xs="6" md="6" lg="6">
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="setpointOx" className="m-0">Set Point</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="setpointOx" defaultValue={this.state.setpointOx}  onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>       
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOn" className="m-0">Time On</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input  id="timeOn" defaultValue={this.state.timeOn}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Min</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>                                                                    
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="maxOx" className="m-0">Alarma Alta de Oxígeno</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="maxOx" defaultValue={this.state.maxOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>                                                                                 
                                                                            </FormGroup>
                                                                            </Col>

                                                                            <Col xs="6" md="6" lg="6">
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="histeresisOx" className="m-0">Histeresis</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="histeresisOx" defaultValue={this.state.histeresisOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup   className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOff" className="m-0">Time Off</Label>
                                                                                    <InputGroup >                                                                                      
                                                                                        <Input  id="timeOff" defaultValue={this.state.timeOff}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Min</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="minOx" className="m-0">Alarma Baja de Oxígeno</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="minOx" defaultValue={this.state.minOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                            </Col>

                                                                        </Row>
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>

                                                            <Card>
                                                                <CardBody>
                                                                <FormGroup   className="mt-2" >
                                                                    <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Estado</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <InputGroup style={{paddingTop:5}}>
                                                                            <label className="switch">
                                                                                <input type="checkbox" id="estadoTK" checked={this.state["estadoTK"]} onClick={(()=>{
                                                                                    this.setState({["estadoTK"]:this.state["estadoTK"]==0?1:0})
                                                                                    sw=false;
                                                                                })} />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                                {/* {
                                                                                    this.state.nCard.map((c,i)=>{
                                                                                        if(i==0)
                                                                                            if(sw){
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:false})
                                                                                                                sw=false;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }else{
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:true})
                                                                                                                sw=true;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }
                                                                                    })
                                                                                }   */}
                                                                                <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Habilitar TK</Label>
                                                                                </InputGroup>
                                                                        </Col>
                                                                    </Row>
                                                                    <hr />
                                                                    {/* <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Control</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                                <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Automatico</Label>
                                                                                    </InputGroup>
                                                                                    
                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw2){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw2=true;
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual On</Label>
                                                                                    </InputGroup>      

                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw3){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw3=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw3=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual Off</Label>
                                                                                    </InputGroup>
                                                                            </Col>
                                                                        </Row> */}
                                                                         
                                                                        </FormGroup>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Row style={{width:`${100}%`}}>
                                                                <Col xs="6" md="6" lg="6"><Button color="primary" disabled={habilitado } onClick={() => {
                                                                    modificacion_general=true;
                                                                    this.almacenarConfig();
                                                                }}>Aplicar a Modulo {this.props.n_modulo}</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="primary" disabled={habilitado }   onClick={() => this.almacenarConfig()}>Guardar</Button>{' '}</Col>
                                                            </Row>
                                                    </ModalFooter>
                                                    </BlockUi>
                                        </Modal>
            </Fragment>
        )
    }
}