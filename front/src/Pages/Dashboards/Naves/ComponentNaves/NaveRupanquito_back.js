import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';
import {
    toast

} from 'react-toastify';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    Badge,ButtonGroup,Breadcrumb, BreadcrumbItem
} from 'reactstrap';
import Media from 'react-media';
//CustomInput,
import '../../../../assets/style.css'

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';

import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';
import { SizeMe } from 'react-sizeme';
import { number } from 'prop-types';

import '../../../../assets/style.css'



//console.log("props: "+this.props.Mensaje)

//console.log("props: "+JSON.stringify(this.props));



let TK101 = "6047782669e89545ec0047e1";
let TK102 = "6047782669e89545ec0047e2";
let TK103 = "6047782669e89545ec0047e3";
let TK104 = "6047782669e89545ec0047e4";
let TK105 = "6047782669e89545ec0047e5";
let TK106 = "6047782669e89545ec0047e6";
let TK107 = "6047782669e89545ec0047e7";
let TK108 = "6047782669e89545ec0047e8";
let TK109 = "6047782669e89545ec0047e9";
let TK110 = "6047782669e89545ec0047ea";
let TK111 = "5d7a7403b123470168990ab3";//no va
let TK112 = "5d7a7408b123470168990ab4";//no va
// const S3 = "5ced38e9197f5732a44eb517";
let Z2 = "6047826169e89545ec004843";
// const S3 = "5ced38e9197f5732a44eb517";

let sw=false;
let c_sw=false;
let c_sw2=false;
let c_sw3=false;
let visible="block";

let check=false;
let modificacion_general=false;
let habilitado =false;
//localStorage.setItem("check1", true);
//localStorage.removeItem("check1");



export default class IndexZonaParcelaLarvas extends Component {   
    constructor() {
        super();
        
        this.state = {   
            myDataChart1: [],   
            myDataChart2: [], 
            myDataChart3: [],
            myDataChart4: [],
            myDataChart5: [],
            myDataChart6: [],
            myDataChart7: [],
            myDataChart8: [],
            myDataChart9: [],
            myDataChart10: [],
            myDataChart11: [],
            myDataChart12: [],
            myCardOpacity1:0,
            myCardOpacity2:0,
            myCardOpacity3:0,
            myCardOpacity4:0,
            myCardOpacity5:0,
            myCardOpacity6:0,
            myCardOpacity7:0,
            myCardOpacity8:0,
            myCardOpacity9:0,
            myCardOpacity10:0,
            myCardOpacity11:0,
            myCardOpacity12:0,
            misTag: [],     
            isLoading: false,
            error: null,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
            
            bitValTK101:0,
            bitVenTK101:0,
            // bitValTK102:0,
            // bitVenTK102:0,
            // bitValS3:0,
            // bitVenS3:0,
            mySalaModal:0,
            MyTags:[],
            nCard:[1,3,5,7,9,11,2,4,6,8,10,12],
            check:false,
        }
        this.loadDataChar = this.loadDataChar.bind(this);
        this.loadDataTag = this.loadDataTag.bind(this);
        this.toggleModal1 = this.toggleModal1.bind(this);
        this.almacenarConfig = this.almacenarConfig.bind(this);
        this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }
    
  
    componentDidMount = () => {
     //   window.location.reload();
        check=false;
        this.getTags();
        const intervaloRefresco =240000;
        this.setState({ isLoading: true });
        this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        this.loadDataChar(); 
        this.loadDataTag();
        this.intervalIdtag = setInterval(() => this.loadDataTag(),intervaloRefresco);
        const hostname = window && window.location && window.location.hostname;
        if(hostname === 'ihc.idealcontrol.cl'){
            habilitado = true;
        }
    }

    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
       clearInterval(this.intervalConfirmacion);
       clearInterval(this.intervalIdtag);
    }  

    loadDataTag = () => {
        const endpointmistag = `${API_ROOT}/zone/${Z2}/tag`;
       // console.log(endpointmistag);
        const token = "tokenfalso";
        axios
        .get(endpointmistag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const mystag = response.data.data;
            //console.log(mystag);  

           const bitValTK101 =  mystag.filter((tag) => tag.nameAddress === "D100TK101N1")[0].lastValue;
           const bitVenTK101 =  mystag.filter((tag) => tag.nameAddress === "D101TK101N1")[0].lastValue;
           //const bitValTK102 =  mystag.filter((tag) => tag.nameAddress === "D111TK101N1")[0].lastValue;
        //    const bitVenTK102 =  mystag.filter((tag) => tag.nameAddress === "bitVenTK102")[0].lastValue;
        //    const bitValS3 =  mystag.filter((tag) => tag.nameAddress === "bitValS3")[0].lastValue;
        //    const bitVenS3 =  mystag.filter((tag) => tag.nameAddress === "bitVenS3")[0].lastValue;
         //}  console.log(bitValS1 + " - " + bitVenS1 + " - "+ bitValTK102 + " - " + bitVenTK102 + " - " + bitValS3 + " - " + bitVenS3 + " - ") ;
         //console.log("BitVal "+bitValTK101);

           if (this.state.bitValTK101 !== bitValTK101 )
                this.setState({bitValTK101 }); 
            if (this.state.bitVenTK101 !== bitVenTK101 )      
                this.setState({bitVenTK101 }); 
            // if (this.state.bitValTK102 !== bitValTK102 )   
            //     this.setState({bitValTK102 }); 
            // if (this.state.bitVenTK102 !== bitVenTK102 )          
            //     this.setState({bitVenTK102 }); 
            // if (this.state.bitValS3 !== bitValS3 )   
            //    this.setState({bitValS3 });
            // if (this.state.bitVenS3 !== bitVenS3 )   
            //     this.setState({bitVenS3 }); 



            //console.log(this.state.bitValS1);

         
        })
        .catch(error => {
          console.log(error);
        });


 
    }

    getTags=()=>{
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";

        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {

            let MyTags = response.data.data;

           this.setState({ MyTags });   
        })
        .catch(error => {
          console.log(error);
        });
    }

    // getAxios=(api1,api2,token)=>{
    //     return axios.all([
    //         axios.get(API1, {
    //             headers: {  
    //               'Authorization': 'Bearer ' + token
    //             }
    //           }),
    //         axios.get(API2, {
    //             headers: {  
    //               'Authorization': 'Bearer ' + token
    //             }
    //           })
    //       ])
    //       .then(axios.spread((data1,data2)=>{
    //         console.log("1"+data1);
    //         console.log("2"+data2);
    //         const myDataChart1 = data1.data.data;
    //         const myDataChart2 = data2.data.data;
    //         this.setState({ myDataChart1,myDataChart2 })
    //       }))
    //       .catch(error => console.log(error));
    // }

    loadDataChar = () => {
        //var date = moment().subtract(4, 'hours');
        if(!this.state.check){
            var date = moment().subtract(1, 'hours');
        }else{
            var date = moment().subtract(1, 'hours');
        }
        
        var minute = date.minutes();
        //alert(date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm'))

        let now = new Date(); 
        const f1 = date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
      
        const API1 = `${API_ROOT}/tag/location/${TK101}/${f1}/${f2}`;
        const API2 = `${API_ROOT}/tag/location/${TK102}/${f1}/${f2}`;
        const API3 = `${API_ROOT}/tag/location/${TK103}/${f1}/${f2}`;
        const API4 = `${API_ROOT}/tag/location/${TK104}/${f1}/${f2}`;
        const API5 = `${API_ROOT}/tag/location/${TK105}/${f1}/${f2}`;
        const API6 = `${API_ROOT}/tag/location/${TK106}/${f1}/${f2}`;
        const API7 = `${API_ROOT}/tag/location/${TK107}/${f1}/${f2}`;
        const API8 = `${API_ROOT}/tag/location/${TK108}/${f1}/${f2}`;
        const API9 = `${API_ROOT}/tag/location/${TK109}/${f1}/${f2}`;
        const API10 = `${API_ROOT}/tag/location/${TK110}/${f1}/${f2}`;
        const API11 = `${API_ROOT}/tag/location/${TK111}/${f1}/${f2}`;
        const API12 = `${API_ROOT}/tag/location/${TK112}/${f1}/${f2}`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";

        axios.all([
            axios.get(API1, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API2, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API3, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API4, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API5, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API6, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API7, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API8, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API9, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API10, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API11, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(API12, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
            axios.get(EndPointTag, {
                headers: {  
                  'Authorization': 'Bearer ' + token
                }
              }),
          ])
          .then(axios.spread((data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12,dataAll)=>{
            //console.log(data1.data.data.concat(data2.data.data.concat(data3.data.data)));
            //console.log(data1.data.data);
            // const myDataChart1 = data1.data.data.concat(data2.data.data
            //                                     .concat(data3.data.data
            //                                     .concat(data4.data.data
            //                                     .concat(data5.data.data
            //                                     .concat(data6.data.data
            //                                     .concat(data7.data.data
            //                                     .concat(data8.data.data
            //                                     .concat(data9.data.data
            //                                     .concat(data10.data.data
            //                                     .concat(data11.data.data
            //                                     .concat(data12.data.data
            //                                     )))))))))));
            const tags=dataAll.data.data;

            let myDataChart1 = data1.data.data;
            let myDataCard1=0;

            if(tags.filter((tag) => tag.nameAddress === "M6001TK101N1")[0].state!=-1){
                //console.log(myDataChart1.filter((tag) => tag.nameAddress === "M6001TK101N1")[0].lastValue)
                try{
                    myDataCard1=tags.filter((tag) => tag.nameAddress === "M6001TK101N1")[0].lastValue;
                }catch(e){
                    myDataCard1=0;
                }
            }
            
            const myDataChart2 = data2.data.data;
            let myDataCard2=0;
            if(tags.filter((tag) => tag.nameAddress === "M6002TK102N1")[0].state!=-1){
                try{
                    myDataCard2=tags.filter((tag) => tag.nameAddress === "M6002TK102N1")[0].lastValue;
                }catch(e){
                    myDataCard2=0;
                }
            }
            
            const myDataChart3 = data3.data.data;
            let myDataCard3=0;
            if(tags.filter((tag) => tag.nameAddress === "M6003TK103N1")[0].state!=-1){
                try{
                    myDataCard3=tags.filter((tag) => tag.nameAddress === "M6003TK103N1")[0].lastValue;
                }catch(e){
                    myDataCard3=0;
                }
            }

            const myDataChart4 = data4.data.data;
            let myDataCard4=0;
            if(tags.filter((tag) => tag.nameAddress === "M6004TK104N1")[0].state!=-1){
                try{
                    myDataCard4=tags.filter((tag) => tag.nameAddress === "M6004TK104N1")[0].lastValue;
                }catch(e){
                    myDataCard4=0;
                }
            }
            
            const myDataChart5 = data5.data.data;
            let myDataCard5=0;
            if(tags.filter((tag) => tag.nameAddress === "M6005TK105N1")[0].state!=-1){
                try{
                    myDataCard5=tags.filter((tag) => tag.nameAddress === "M6005TK105N1")[0].lastValue;
                }catch(e){
                    myDataCard5=0;
                }
            }
            
            const myDataChart6 = data6.data.data;
            let myDataCard6=0;
            if(tags.filter((tag) => tag.nameAddress === "M6006TK106N1")[0].state!=-1){
                try{
                    myDataCard6=tags.filter((tag) => tag.nameAddress === "M6006TK106N1")[0].lastValue;
                }catch(e){
                    myDataCard6=0;
                }
            }
           
            const myDataChart7 = data7.data.data;
            let myDataCard7=0;
            if(tags.filter((tag) => tag.nameAddress === "M6007TK107N1")[0].state!=-1){
                try{
                    myDataCard7=tags.filter((tag) => tag.nameAddress === "M6007TK107N1")[0].lastValue;
                }catch(e){
                    myDataCard7=0;
                }
            }
             
            const myDataChart8 = data8.data.data;
            let myDataCard8=0;
            if(tags.filter((tag) => tag.nameAddress === "M6008TK108N1")[0].state!=-1){
                try{
                    myDataCard8=tags.filter((tag) => tag.nameAddress === "M6008TK108N1")[0].lastValue;
                }catch(e){
                    myDataCard8=0;
                }
            }
            
            const myDataChart9 = data9.data.data;
            let myDataCard9=0;
            if(tags.filter((tag) => tag.nameAddress === "M6009TK109N1")[0].state!=-1){
                try{
                    myDataCard9=tags.filter((tag) => tag.nameAddress === "M6009TK109N1")[0].lastValue;
                }catch(e){
                    myDataCard9=0;
                }
            }
           
            const myDataChart10 = data10.data.data;
            let myDataCard10=0;
            if(tags.filter((tag) => tag.nameAddress === "M6010TK110N1")[0].state!=-1){
                try{
                    myDataCard10=tags.filter((tag) => tag.nameAddress === "M6010TK110N1")[0].lastValue;
                }catch(e){
                    myDataCard10=0;
                }
            }
            let myDataChart11 = data11.data.data;
            let myDataCard11=0;
            // if(tags.filter((tag) => tag.nameAddress === "M6011TK111N1")[0].state!=-1){
            //     try{
            //         myDataCard11=tags.filter((tag) => tag.nameAddress === "M6011TK111N1")[0].lastValue;
            //     }catch(e){
            //         myDataCard11=0;
            //     }
            // }
            let myDataChart12 = data12.data.data;
            let myDataCard12=0;
            // if(tags.filter((tag) => tag.nameAddress === "M6012TK112N1")[0].state!=-1){
            //     try{
            //         myDataCard12=tags.filter((tag) => tag.nameAddress === "M6012TK112N1")[0].lastValue;
            //     }catch(e){
            //         myDataCard12=0;
            //     }
            // }

            


             //const myDataCard1=myDataChart1.filter((tag) => tag.nameAddress === "M6001TK101N1")[0].lastValue;
            //const myDataCard2=myDataChart2.filter((tag) => tag.nameAddress === "M6002TK102N1")[0].lastValue;
            //const myDataCard3=myDataChart3.filter((tag) => tag.nameAddress === "M6003TK103N1")[0].lastValue;
            //const myDataCard4=myDataChart4.filter((tag) => tag.nameAddress === "M6004TK104N1")[0].lastValue;
            //const myDataCard5=myDataChart5.filter((tag) => tag.nameAddress === "M6005TK105N1")[0].lastValue;
            //const myDataCard6=myDataChart6.filter((tag) => tag.nameAddress === "M6006TK106N1")[0].lastValue;
            //const myDataCard7=myDataChart7.filter((tag) => tag.nameAddress === "M6007TK107N1")[0].lastValue;
            //const myDataCard8=myDataChart8.filter((tag) => tag.nameAddress === "M6008TK108N1")[0].lastValue;
            //const myDataCard9=myDataChart9.filter((tag) => tag.nameAddress === "M6009TK109N1")[0].lastValue;
            //const myDataCard10=myDataChart10.filter((tag) => tag.nameAddress === "M6010TK110N1")[0].lastValue;
            //const myDataCard11=myDataChart11.filter((tag) => tag.nameAddress === "M6011TK111N1")[0].lastValue;
            //const myDataCard12=myDataChart12.filter((tag) => tag.nameAddress === "M6012TK112N1")[0].lastValue;
            //console.log(data11.data.data);

            this.setState({ myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,
                myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,
                myDataCard1,myDataCard2,myDataCard3,myDataCard4,myDataCard5,myDataCard6,myDataCard7,
                myDataCard8,myDataCard9,myDataCard10,myDataCard11,myDataCard12,isLoading: false })
          }))
          .catch(error => {
            console.log(error);
            const myDataChart1=[];
            const myDataChart2=[];
            const myDataChart3=[];
            const myDataChart4=[];
            const myDataChart5=[];
            const myDataChart6=[];
            const myDataChart7=[];
            const myDataChart8=[];
            const myDataChart9=[];
            const myDataChart10=[];
            const myDataChart11=[];
            const myDataChart12=[];

            this.setState({ myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,
                myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,isLoading: false })
          });
        
    }

    getMinY = (data)=> {
        return data.reduce((min, p) => p.lastValue < min ? p.lastValue  : min, data[0].lastValue );
    }
    getDataModal(myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,
        myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,TK){
        switch (TK) {
                case "101":
                    return myDataChart1
                case "102":
                    return myDataChart2
                case "103":
                    return myDataChart3                  
                case "104":
                    return myDataChart4                   
                case "105":
                    return myDataChart5                  
                case "106":
                    return myDataChart6                  
                case "107":
                    return myDataChart7                 
                case "108":
                    return myDataChart8                   
                case "109":
                    return myDataChart9             
                case "110":
                    return myDataChart10                  
                case "111":
                    return myDataChart11                
                case "112":
                    return myDataChart12                  
        }
        
    }

    toggleModal1(sala) {
        //console.log(sala);
        visible="block";
        modificacion_general=false;

        if (this.state.modal1 === false){ 
        
            
                let sa = "";
                let num="";

                switch (sala) {  
                    case "101":
                        sa = "101";
                        num="01";
                    break;
                    case "102":
                        sa = "102";
                        num="02";
                    break;
                    case "103":
                        sa = "103";
                        num="03";                  
                    break;
                    case "104":
                        sa = "104";
                        num="04";                   
                    break;
                    case "105":
                        sa = "105";
                        num="05";                  
                    break;
                    case "106":
                        sa = "106";
                        num="06";                   
                    break;
                    case "107":
                        sa = "107";
                        num="07";                 
                    break;
                    case "108":
                        sa = "108";
                        num="08";                    
                    break;
                    case "109":
                        sa = "109";
                        num="09";              
                    break;
                    case "110":
                        sa = "110";
                        num="10";                    
                    break;
                    case "111":
                        sa = "111";
                        num="11";                  
                    break;
                    case "112":
                        sa = "112";
                        num="12";                    
                    break;

                    default:
                        sa = "No definido";
                    break;

                }   
                this.setState({
                    mySalaModal:sa
                });

                let mysala = "TK" + sa;   

                const { myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10
                    ,myDataChart11,myDataChart12,MyTags} = this.state;
                
                const tags=this.getDataModal(myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10
                    ,myDataChart11,myDataChart12,sa);

                    try{
                        console.log(MyTags.filter((tag) => tag.nameAddress === `M6101TK101N1`)[0].lastValue==1)
                        if(MyTags.filter((tag) => tag.nameAddress === `M63${num}${mysala}N1`)[0].lastValue==1){
                            c_sw=true;
                        }else{
                            c_sw=false;
                        }
                        if(MyTags.filter((tag) => tag.nameAddress === `M61${num}${mysala}N1`)[0].lastValue==1){
                            c_sw2=true;
                        }else{
                            c_sw2=false;
                        }
                        if(MyTags.filter((tag) => tag.nameAddress === `M62${num}${mysala}N1`)[0].lastValue==1){
                            c_sw3=true;
                        }else{
                            c_sw3=false;
                        }
                    }catch(e){
                        console.log(e)
                    }


                    //console.log(tags.filter((tag) => tag.nameAddress === "maxTempS1V"))
                    //console.log(MyTags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`)[0].lastValue==1);
                        try{
                            if(MyTags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`)[0].lastValue==1){
                                sw=true;
                            // console.log("si "+sw);
                            }else{
                                sw=false;
                            // console.log("no "+sw);
                            }
                        }catch(e){
                            console.log(e)
                            visible="none";
                            sw=false;
                        }
                        let num2=0;
                        num>9?num2=num:num2=num[1];
                        //console.log(`M60${num}${mysala}N1`);
                        
                        try{
                            console.log("1"+MyTags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`))
                            console.log("2"+tags.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N1`)[0].lastValue)
                            if(sa!="No definido"){
                                this.setState({
                                    histeresisOxs : tags.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N1`),
                                    setpointOxs: tags.filter((tag) => tag.nameAddress === `D2${num}04TK${sa}N1`),
                                    maxOxs : tags.filter((tag) => tag.nameAddress === `D2${num}06TK${sa}N1`),
                                    minOxs: tags.filter((tag) => tag.nameAddress === `D2${num}07TK${sa}N1`), 
                                    timeOffs : tags.filter((tag) => tag.nameAddress === `D2${num}09TK${sa}N1`),  
                                    timeOns: tags.filter((tag) => tag.nameAddress === `D2${num}08TK${sa}N1`),
                                    habilitadoTK: MyTags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`),      
                                    
                                    histeresisOx : tags.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N1`)[0].lastValue,
                                    setpointOx: tags.filter((tag) => tag.nameAddress === `D2${num}04TK${sa}N1`)[0].lastValue,
                                    maxOx : tags.filter((tag) => tag.nameAddress === `D2${num}06TK${sa}N1`)[0].lastValue,
                                    minOx: tags.filter((tag) => tag.nameAddress === `D2${num}07TK${sa}N1`)[0].lastValue,
                                    timeOff : tags.filter((tag) => tag.nameAddress === `D2${num}09TK${sa}N1`)[0].lastValue,
                                    timeOn: tags.filter((tag) => tag.nameAddress === `D2${num}08TK${sa}N1`)[0].lastValue,
                                    estadoTK: MyTags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`)[0].lastValue,
                                    
                                    OxdTK: tags.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N1`),
                                    
                                    modal1: !this.state.modal1,
                                    misTag: tags
                                });
                            }
                        }catch(e){
                            console.log(e);
                            visible="none";

                            const EndPointTag = `${API_ROOT}/tag`;
                            const token = "tokenfalso";
                            axios
                            .get(EndPointTag, {headers: {'Authorization': 'Bearer ' + token}})
                            .then(response => {
                                const tags = response.data.data;
                                console.log(tags.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N1`));
                                this.setState({
                                    habilitadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N1`),  

                                    OxdTK: tags.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N1`),
                                    
                                    modal1: !this.state.modal1,

                                    misTag: tags
                                })
                            })
                            .catch(error => {
                            console.log(error);
                            });

                        }
                   
                    
                    
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }


    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });   
        
        
        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            console.log(response);
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );      
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                this.componentDidMount();
                // this.loadDataTag();
                // this.loadDataChar();
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            }else if (this.state.intentosGuardado >= 10)
            {
                  //this.componentDidMount();
                  this.loadDataTag();
                  this.loadDataChar();
                  toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false}); 
                  clearInterval(this.intervalConfirmacion);
            }
        })
        .catch(error => {
          console.log(error);
        });

    }


    // confirmacionTermino= () => {
    //     this.setState({
    //         intentosGuardado: this.state.intentosGuardado + 1
    //     });   
        
        
    //     //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
    //     const EndPointTag = `${API_ROOT}/tag`;
    //     const token = "tokenfalso";
    //     axios
    //     .get(EndPointTag, {
    //       headers: {  
    //         'Authorization': 'Bearer ' + token
    //       }
    //     })
    //     .then(response => {
    //         console.log(response);
    //         console.log("INTENTOS: " + this.state.intentosGuardado);
    //         const tags = response.data.data;
    //         const TagWrites  = tags.filter((tag) => tag.write === true );      
    //         if (TagWrites.length === 0){
    //             clearInterval(this.intervalConfirmacion);
    //             this.setState({blocking1: false});
    //             toast['success']('Almacenado Correctamente', { autoClose: 4000 })
    //         }else{
    //             if (this.state.intentosGuardado === 10) 
    //             {
    //               this.loadDataChar();
    //               toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
    //               this.setState({blocking1: false}); 
    //               clearInterval(this.intervalConfirmacion);
    //             }
    //         }
           
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });

    // }

    updateAllConfig=(content,config,include)=>{
        let tags_capture="";
        tags_capture=this.state.MyTags.filter((data)=>{
            return String(data.shortName).includes(include)
        })
            .map((data)=>{
                let EndPointTag = `${API_ROOT}/tag/${data._id}`;
                axios.put(EndPointTag, content, config)
                .then(response => {
                });
                return data
            })
    }

    updateAllConfigAlert=(content,config,include,equals)=>{
        let tags_capture="";
        tags_capture=this.state.MyTags.filter((data)=>{
            if(String(data.shortName).includes(include)){
                return data.address==equals
            }
        })
            .map((data)=>{
                let EndPointTag = `${API_ROOT}/tag/${data._id}`;
                axios.put(EndPointTag, content, config)
                .then(response => {
                });
                return data
            })
    }

    //address,data,config,EndPointTag,content9,content10,content11
    updateTagControl=(address,data,config,EndPointTag,content9,content10,content11)=>{
        // console.log("M63"+address[1]+address[2]+"TK"+address);
        // console.log("M61"+address[1]+address[2]+"TK"+address==data.nameAddress);
        //Automatico
        if("M63"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT}/tag/${data._id}`;
            axios.put(EndPointTag, content9, config)
            .then(response => {
            });
        }
        //Manual on
        else if("M61"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT}/tag/${data._id}`;
            axios.put(EndPointTag, content10, config)
            .then(response => {
            });
        }
        //Manual off
        else if("M62"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT}/tag/${data._id}`;
            axios.put(EndPointTag, content11, config)
            .then(response => {
            });
        }
    }

    almacenarConfig = () => {
        console.log(sw);
        this.setState({blocking1: true,intentosGuardado:0});

       this.intervalConfirmacion = setInterval(() => this.confirmacionTermino(),3000);
       
       const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag="";
       const  content1 = {
            "lastValue": this.state.histeresisOx,
            "lastValueWrite":this.state.histeresisOx,
            "write":true
        }  
        const  content2 = {
            "lastValue": this.state.setpointOx,
            "lastValueWrite":this.state.setpointOx,
            "write":true
        } 
        const  content3 = {
            "lastValue": this.state.maxOx,
            "lastValueWrite":this.state.maxOx,
            "write":true
        } 
        const  content4 = {
            "lastValue": this.state.minOx,
            "lastValueWrite":this.state.minOx,
            "write":true
        }
        const  content4_5 = {
            "alertMax": this.state.maxOx,
            "alertMin":this.state.minOx,
            "write":true
        }
        const  content5 = {
            "lastValue": this.state.timeOff,
            "lastValueWrite":this.state.timeOff,
            "write":true
        }  
        const  content6 = {
            "lastValue": this.state.timeOn,
            "lastValueWrite":this.state.timeOn,
            "write":true
        }

       if(modificacion_general){
            this.getTags();
            console.log("modificacion All")
            
            console.log(this.state.MyTags)
            let tags_capture="";
            let cont=0;
            const get_address=this.state.habilitadoTK[0].nameAddress.split('TK');
            let address=get_address[1];
            console.log(address[0]);
            while(cont<=12){
                if(cont<10){
                    this.updateAllConfig(content1, config, 'Histeresis Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content2, config, 'Set Point  Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content3, config, 'Max Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content4, config, 'Min Ox  TK'+address[0]+'0'+cont);

                    let address_num=cont*100;
                    this.updateAllConfigAlert(content4_5, config,'Ox TK'+address[0]+'0'+cont, address_num);

                    this.updateAllConfig(content5, config, 'Time Off Val TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content6, config, 'Time On Val TK'+address[0]+'0'+cont);
                }else{
                    this.updateAllConfig(content1, config, 'Histeresis Ox TK'+address[0]+cont);
                    this.updateAllConfig(content2, config, 'Set Point  Ox TK'+address[0]+cont);
                    this.updateAllConfig(content3, config, 'Max Ox TK'+address[0]+cont);
                    this.updateAllConfig(content4, config, 'Min Ox  TK'+address[0]+cont);

                    let address_num=cont*100;
                    this.updateAllConfigAlert(content4_5, config,'Ox TK'+address[0]+cont, address_num);

                    this.updateAllConfig(content5, config, 'Time Off Val TK'+address[0]+cont);
                    this.updateAllConfig(content6, config, 'Time On Val TK'+address[0]+cont);
                }
                cont++
            }
       }     
       else{
        if(visible!="none"){
            console.log("i "+this.state.histeresisOxs[0]._id);
            EndPointTag = `${API_ROOT}/tag/${this.state.histeresisOxs[0]._id}`; 

            
            axios.put(EndPointTag, content1, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.setpointOxs[0]._id}`; 
            
            axios.put(EndPointTag, content2, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.maxOxs[0]._id}`; 
            
            axios.put(EndPointTag, content3, config)
            .then(response => {
                
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.minOxs[0]._id}`; 
            
            axios.put(EndPointTag, content4, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.OxdTK[0]._id}`; 
            
            axios.put(EndPointTag, content4_5, config)
            .then(response => {
                
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.timeOffs[0]._id}`; 
            
            axios.put(EndPointTag, content5, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT}/tag/${this.state.timeOns[0]._id}`; 
            
            axios.put(EndPointTag, content6, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });
        }
        
        EndPointTag = `${API_ROOT}/tag/${this.state.habilitadoTK[0]._id}`; 
        let content7="";
        if(sw==true){
            content7 = {
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true            
            }
        }else{
            content7 = {
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true            
            }
        }

        axios.put(EndPointTag, content7, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });
        

        const get_address=this.state.habilitadoTK[0].nameAddress.split('TK');
        let address=get_address[1];
        let content8="";//habilitado
        if(sw){
            content8={
               "state":1
           }
       }else{
            content8={
               "state":-1
           }
       }
        let content9="";//automatico
        if(c_sw){
             content9={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content9={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        let content10="";//manual on
        if(c_sw2){
             content10={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content10={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        let content11="";//manual off
        if(c_sw3){
             content11={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content11={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        console.log(address);
        let tags_capture="";

            if(address=="101N1"){
                // console.log(this.state.MyTags);
                // console.log(this.state.MyTags.filter((tag)=>{
                //     let name_addr=String(tag.nameAddress);
                //     return name_addr.includes('101N1');
                // }));
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                })
            }
            else if(address=="102N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="103N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="104N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    //  console.log("actualizado " + response.status);
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="105N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="106N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="107N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="108N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="109N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="110N1"){
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address=="111N1")
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            else if(address=="112N1")
                tags_capture=this.state.MyTags.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            else
                console.log("Rayos")
        
    }
        this.componentDidMount();
        this.loadDataTag();
 
    }
 
    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => { 
        // console.log(event.target.defaultChecked);  
        this.setState( { 
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }

    //genera cada boton para el Oxs, Temp y Oxd
    getButton = (data,i,n)=>{
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        return<div className="widget-chart widget-chart-hover  p-0 p-0 ">
             <Button 
                onClick={ e => {
                        try{
                            if(n==0)
                            this.slider1.slickGoTo(i)
                            else if(n==1)
                                this.slider2.slickGoTo(i)
                            else if(n==2)
                                this.slider3.slickGoTo(i)
                            else if(n==3)
                                this.slider4.slickGoTo(i)
                            else if(n==4)
                                this.slider5.slickGoTo(i)
                            else if(n==5)
                                this.slider6.slickGoTo(i)
                            else if(n==6)
                                this.slider7.slickGoTo(i)
                            else if(n==7)
                                this.slider8.slickGoTo(i)
                            else if(n==8)
                                this.slider9.slickGoTo(i)
                            else if(n==9)
                                this.slider10.slickGoTo(i)
                            else if(n==10)
                                this.slider11.slickGoTo(i)
                            else if(n==11)
                                this.slider12.slickGoTo(i)
                        }catch(e){

                        }
                        
                    
                    // console.log("position "+n);
                    // console.log("slickGo "+i);
                }}
                key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                                                                                            
                {data.measurements[data.measurements.length-1].value   } 
                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                </div>
                <div className="widget-subheading">
                {data.name}
                </div>
            </Button>                                              
        </div>
    }
    
    //genera el grafico de Oxs, Temp y Oxd
    getChart = (data,i)=>{
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if(this.state.check)
            return <div className="widget-chart widget-chart2 text-left p-0">
            <div className="widget-chat-wrapper-outer">
                <div className="widget-chart-content widget-chart-content-lg  p-2">
                    <div className="widget-chart-flex ">
                        <div
                            className="widget-title opacity-9 text-muted text-uppercase">
                            {data.name}
                        </div>
                        
                        {/* <div className="btn-actions-pane-right text-capitalize pr-2" > 
                                <div className={cx("divfloatleft mr-2",this.state.bitVenTK101===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                    <img width={23} src={ventilador} alt="" />

                                </div>
                                <div className={cx("divfloatleft mr-2", this.state.bitValTK101===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                    <img width={23} src={valvula} alt="" />

                                </div>                                                                               
                        </div> */}
                    </div>

                    <div className="widget-numbers p-1 m-0">
                        <div className="widget-chart-flex">
                            <div>
                                
                                {data.measurements[data.measurements.length-1].value}
                                <small className="opacity-5 pl-1 size_unidad3">  {data.unity}</small>
                            </div>
                        </div>
                    </div> 
                    
                <div className=" opacity-8 text-focus pt-0">
                    <div className=" opacity-5 d-inline">
                    Max
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                    
                        <span className="pl-1 size_prom">
                                {                                             
                                data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                }
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline  ml-2">
                    Prom
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                        <span className="pl-1 size_prom">
                            {
                                ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                            }
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline ml-2">
                    Min
                    </div>
                    <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                        <span className="pl-1 size_prom">
                        {                                             
                            data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                            }
                        </span>
                    </div>
                    
                </div> 
            

                </div>

                {/* <div className="d-inline text-secondary pr-1">                                                                                                                                                                 
                        <span className="pl-1">
                                {data.dateTimeLastValue}

                        </span>
            </div> */}


                <div
                    className="widget-chart-wrapper he-auto opacity-10 m-0">
                    <ResponsiveContainer height={150} width='100%'>

                        <AreaChart data={data.measurements}
                                // animationDuration={1}
                                isAnimationActive = {false}
                                margin={{
                                    top: 0,
                                    right:10,
                                    left: -30,
                                    bottom: 0
                                }}>
                            
                                <Tooltip                                                                                                        
                                            labelFormatter={function(value) {
                                                return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                }}
                                            formatter={function(value, name) {
                                            return `${value}`;
                                            }}
                                        />
                                <defs>
                                    <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                        <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                    </linearGradient>
                                </defs>
                                <YAxis                                                                                    
                                tick={{fontSize: '10px'}}
                                // domain={['dataMin - 4','dataMax + 4']}
                                />
                                <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                <XAxis
                                        dataKey={'dateTime'}                                                                              
                                        hide = {false}
                                        tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                        tick={{fontSize: '10px'}}
                                        />
                                <Area type='monotoneX' dataKey='value'
                            
                                    stroke={miscolores[i]}
                                    strokeWidth='3'
                                    fillOpacity={1}
                                    fill={"url(#colorPv" + i + ")"}/>
                            </AreaChart>

                    </ResponsiveContainer>
                </div>
            </div>
        </div>
    }

    //realiza los filtro y la llamada a generar el button para los 3 tags de la card
    getViewButton=(array,i,n)=>{
        return array.filter((data)=>{
            if(i==n)
            {
               const num=i+1
               //identifica si el numero es mayor a 9 para cambiar la condicion (evita ej: D101TK1010 que deberia ser D101TK110)
               if(num>9){
                if(data.nameAddress=="D"+num+"00TK1"+num+"N1" || data.nameAddress=="D"+num+"01TK1"+num+"N1" || data.nameAddress=="D"+num+"11TK1"+num+"N1")
                return data
               }else{
                if(data.nameAddress=="D"+num+"00TK10"+num+"N1" || data.nameAddress=="D"+num+"01TK10"+num+"N1" || data.nameAddress=="D"+num+"11TK10"+num+"N1")
                return data
               }
               
            }else{ return "" }   
       }).map((data,i)=>
           <Col sm="4"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
               {this.getButton(data,i,n)}
           </Col>
       )     
    }

    //realiza los filtro y la llamada a generar los graficos para los 3 tags de la card
    getViewChart=(array,i,n)=>{
        return array.filter((data)=>{
            if(i==n)
            {
               const num=i+1
               //identifica si el numero es mayor a 9 para cambiar la condicion (evita ej: D101TK1010 que deberia ser D101TK110)
               if(num>9){
                if(data.nameAddress=="D"+num+"00TK1"+num+"N1" || data.nameAddress=="D"+num+"01TK1"+num+"N1" || data.nameAddress=="D"+num+"11TK1"+num+"N1")
                return data
               }else{
                if(data.nameAddress=="D"+num+"00TK10"+num+"N1" || data.nameAddress=="D"+num+"01TK10"+num+"N1" || data.nameAddress=="D"+num+"11TK10"+num+"N1")
                return data
               }
               
            }else{ return "" }   
       }).map((data,i) =>
                <div key={data._id}>
                    {this.getChart(data,i)}
                </div>  
        ) 
    }

    //realiza un switch por separado para optimizar la busqueda del Slider
    switchViewChart=(array,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)=>{
        return array.map((data)=>{
            if(data==1)
                return this.getViewChart(myDataChart1,i,data-1)
            else if(data==2)
                return this.getViewChart(myDataChart2,i,data-1)
            else if(data==3)
                return this.getViewChart(myDataChart3,i,data-1)
            else if(data==4)
                return this.getViewChart(myDataChart4,i,data-1)
            else if(data==5)
                return this.getViewChart(myDataChart5,i,data-1)
            else if(data==6)
                return this.getViewChart(myDataChart6,i,data-1)
            else if(data==7)
                return this.getViewChart(myDataChart7,i,data-1)
            else if(data==8)
                return this.getViewChart(myDataChart8,i,data-1)
            else if(data==9)
                return this.getViewChart(myDataChart9,i,data-1)
            else if(data==10)
                return this.getViewChart(myDataChart10,i,data-1)
            else if(data==11)
                return this.getViewChart(myDataChart11,i,data-1)
            else if(data==12)
                return this.getViewChart(myDataChart12,i,data-1)
            else
                return ""
        })
    }


    getCard=(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)=>{
      return  <Fragment><Card className="mb-3 mr-20" style={{opacity:`${i==11 || i==10?0:10}`}}>
                            <CardHeader className="card-header-tab">
                                <div className="card-header-title font-size-ls text-capitalize font-weight-normal">
                                    <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                    TK {(i+1>9)?"1"+(i+1):"10"+(i+1)}
                                </div>
                                <div className="btn-actions-pane-right text-capitalize">
                                    <span className="d-inline-block">
                                            <Button color="primary" onClick={() => this.toggleModal1(`${(i+1>9)?"1"+(i+1):"10"+(i+1)}`)}
                                                outline>
                                                <i className="pe-7s-tools btn-icon-wrapper" ></i>
                                            </Button>
                                        
                                    </span>
                                </div>

                                </CardHeader>
                                    {
                                        this.state.nCard.map((data)=>{
                                            if(data==1)
                                                   if(i==0)
                                                        // this.state.myDataCard1=1?"":"0.3"
                                                        return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard1==1?"":"0.3"}`}} >
                                                        {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                        </CardBody>
                                                    else if(i==1)
                                                        return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard2==1?"":"0.3"}`}} >
                                                        {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                        </CardBody>
                                                    else if(i==2)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard3==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==3)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard4==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==4)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard5==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==5)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard6==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==6)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard7==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==7)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard8==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==8)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard9==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    else if(i==9)
                                                            return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard10==1?"":"0.3"}`}} >
                                                            {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                            </CardBody>
                                                    // else if(i==10)
                                                    //         return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard11==1?"":"0.3"}`}} >
                                                    //         {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                    //         </CardBody>
                                                    // else if(i==11)
                                                    //         return <CardBody key={data} className="p-0" style={{opacity:`${this.state.myDataCard12==1?"":"0.3"}`}} >
                                                    //         {this.getCardBody(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                                                    //         </CardBody>
                                                    else
                                                        return "";
                                        })
                                    }     
                            </Card>
                            </Fragment>
    }

    getCardBody=(i,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)=>{
        return <Fragment>
                                <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                    Valor actual de Sondas 
                                    </h6>
                                    <Card className="main-card mb-0">
                                        <div className="grid-menu grid-menu-4col">
                                            <Row className="no-gutters">
                                                         {
                                                             this.state.nCard.map((data)=>{
                                                                        if(data==1)
                                                                            return this.getViewButton(myDataChart1,i,data-1)
                                                                        else if(data==2)
                                                                            return this.getViewButton(myDataChart2,i,data-1)
                                                                        else if(data==3)
                                                                            return this.getViewButton(myDataChart3,i,data-1)
                                                                        else if(data==4)
                                                                            return this.getViewButton(myDataChart4,i,data-1)
                                                                        else if(data==5)
                                                                            return this.getViewButton(myDataChart5,i,data-1)
                                                                        else if(data==6)
                                                                            return this.getViewButton(myDataChart6,i,data-1)
                                                                        else if(data==7)
                                                                            return this.getViewButton(myDataChart7,i,data-1)
                                                                        else if(data==8)
                                                                            return this.getViewButton(myDataChart8,i,data-1)
                                                                        else if(data==9)
                                                                            return this.getViewButton(myDataChart9,i,data-1)
                                                                        else if(data==10)
                                                                            return this.getViewButton(myDataChart10,i,data-1)
                                                                        else if(data==11)
                                                                            return this.getViewButton(myDataChart11,i,data-1)
                                                                        else if(data==12)
                                                                            return this.getViewButton(myDataChart12,i,data-1)
                                                                        else
                                                                            return ""
                                                                    })
                                                         }                                   
                                            </Row>
                                        </div>
                                    </Card>
                                    <div className="p-1 slick-slider-sm mx-auto">

                                    {
                                        this.state.nCard.map((data)=>{
                                                if(this.state.check)
                                                if(data==1)
                                                         if(i==0)
                                                                return <Slider key={data} ref={slider1 => (this.slider1 = slider1)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==1)
                                                                return <Slider key={data} ref={slider2 => (this.slider2 = slider2)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==2)
                                                                return <Slider key={data} ref={slider3 => (this.slider3 = slider3)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==3)
                                                                return <Slider key={data} ref={slider4 => (this.slider4 = slider4)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==4)
                                                                return <Slider key={data} ref={slider5 => (this.slider5 = slider5)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==5)
                                                                return <Slider key={data} ref={slider6 => (this.slider6 = slider6)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==6)
                                                                return <Slider key={data} ref={slider7 => (this.slider7 = slider7)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==7)
                                                                return <Slider key={data} ref={slider8 => (this.slider8 = slider8)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==8)
                                                                return <Slider key={data} ref={slider9 => (this.slider9 = slider9)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==9)
                                                                return <Slider key={data} ref={slider10 => (this.slider10 = slider10)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==10)
                                                                return <Slider key={data} ref={slider11 => (this.slider11 = slider11)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>
                                                         else if(i==11)
                                                                return <Slider key={data} ref={slider12 => (this.slider12 = slider12)} {...settings}>
                                                                {
                                                                    this.switchViewChart(this.state.nCard,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12,i)
                                                                }
                                                                 </Slider>                             
                                                else
                                                    return ""
                                        })
                                    }                                     
                                    

                                    </div>
                                </Fragment>
    }
    
    
    
    render() {
        var this_url=window.location.href;
        var n_nave=this_url.split('/');
        
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10
            ,myDataChart11,myDataChart12,isLoading, error} = this.state;
         
          const settings = {
            autoplaySpeed:6000,
            autoplay: false,        
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       

        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

        let url=window.location;
        let n_modul=String(url).split('modulo/');
        
        return (
            <Fragment>     
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Modulos</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Modulo {n_modul[1]}00</BreadcrumbItem>
                      
                      <Media queries={{ small: { maxWidth:771 },media:{minWidth:772,maxWidth: 1158} }}>
                        {matches =>(
                            <Fragment>
                            {matches.small &&
                                <ButtonGroup style={{marginLeft:`${80}%`,marginTop:-20}}>
                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            
                            {matches.media &&
                                <ButtonGroup style={{marginLeft:`${90}%`,marginTop:-20}}>
                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            {(!matches.media && !matches.small) &&
                                <ButtonGroup style={{marginLeft:`${92}%`,marginTop:-20}}>
                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                    this.setState({check:true})
                                }}>
                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                </Button>
                                <Button style={{width:39}} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                    this.setState({check:false})
                                }}>-</Button>
                                {/* <Button color="primary" outline  onClick={() => {
                                    this.setState({check2:true,check:false})
                                }} >
                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                </Button> */}
                                </ButtonGroup>}
                            
                            </Fragment>
                        )}
                        </Media>
                            
                      </Breadcrumb>

                      {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                      Modulo 100
                                    </div>
                                    <div>
                                    <label className="tooltip-right" style={{marginLeft:10,marginTop:5}}  data-tooltip="Ver Graficos">
                                            <input type="checkbox" checked={check} onClick={(()=>{
                                                if(check){
                                                    check=false;
                                                }else{
                                                    check=true;
                                                }
                                            })} onChange={e => this.inputChangeHandler(e)}></input>
                                            <span className=""></span>
                                    </label> 
                                    </div>
                                </div>
                                <div className="page-title-actions"> 
                                     Alarmado
                                    <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  
                    
                        
                                    Normal
                                    <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>
                                </div>   
                                                
                            </div>
                        </div> */}
                        <Row>
                                    <Modal isOpen={this.state.modal1}>
                                                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion TK {this.state.mySalaModal}</ModalHeader>
                                                    <ModalBody>

                                                    <Row>     
                                                    
                                                        <Col  xs="12" md="12" lg="12">
                                                            <Card className="main-card mb-3">
                                                                <CardBody>
                                                                    <CardTitle>Oxigeno</CardTitle>
                                                                    <Form>
                                                                        <Row>

                                                                            <Col xs="6" md="6" lg="6">
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="setpointOx" className="m-0">Set Point</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="setpointOx" defaultValue={this.state.setpointOx}  onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>       
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOn" className="m-0">Time Off</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input  id="timeOn" defaultValue={this.state.timeOn}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Min</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>                                                                    
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="maxOx" className="m-0">Alarma Alta de Oxígeno</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="maxOx" defaultValue={this.state.maxOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>                                                                                 
                                                                            </FormGroup>
                                                                            </Col>

                                                                            <Col xs="6" md="6" lg="6">
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="histeresisOx" className="m-0">Histeresis</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="histeresisOx" defaultValue={this.state.histeresisOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup   className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOff" className="m-0">Time On</Label>
                                                                                    <InputGroup >                                                                                      
                                                                                        <Input  id="timeOff" defaultValue={this.state.timeOff}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Min</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="minOx" className="m-0">Alarma Baja de Oxígeno</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="minOx" defaultValue={this.state.minOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                            </Col>

                                                                        </Row>
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>

                                                            <Card>
                                                                <CardBody>
                                                                <FormGroup   className="mt-2" >
                                                                    <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Estado</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <InputGroup style={{paddingTop:5}}>
                                                                                {
                                                                                    this.state.nCard.map((c,i)=>{
                                                                                        if(i==1)
                                                                                            if(sw){
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:false})
                                                                                                                sw=false;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }else{
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:true})
                                                                                                                sw=true;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }
                                                                                    })
                                                                                }  
                                                                                <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Habilitar TK</Label>
                                                                                </InputGroup>
                                                                        </Col>
                                                                    </Row>
                                                                    <hr />
                                                                    <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Control</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                                <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Automatico</Label>
                                                                                    </InputGroup>
                                                                                    
                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw2){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw2=true;
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual On</Label>
                                                                                    </InputGroup>      

                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw3){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw3=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw3=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual Off</Label>
                                                                                    </InputGroup>
                                                                            </Col>
                                                                        </Row>
                                                                         
                                                                        </FormGroup>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Row style={{width:`${100}%`}}>
                                                                <Col xs="6" md="6" lg="6"><Button color="primary" disabled={habilitado } onClick={() => {
                                                                    modificacion_general=true;
                                                                    this.almacenarConfig();
                                                                }}>Aplicar a Modulo {`${this.state.mySalaModal[0]}00`}</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="primary" disabled={habilitado }   onClick={() => this.almacenarConfig()}>Guardar</Button>{' '}</Col>
                                                            </Row>
                                                    </ModalFooter>
                                                    </BlockUi>
                                        </Modal>
                            {this.state.nCard.map((c,i)=>{
                            
                            return<Col key={i} sm="6" lg="2" className="animated fadeIn fast">
                                {this.getCard(c-1,settings,myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10,myDataChart11,myDataChart12)}
                            </Col> 

                            })}      

                        </Row>

           
            </Fragment>
        )
    }
}