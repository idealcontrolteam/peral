import React, {Component, Fragment} from 'react';
import IndexZone from './ComponentNaves/Naves'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {checkStatusLogin} from '../../../services/user';
import { Redirect, Route } from 'react-router-dom';




export default class IndexNave extends Component { 
    render() {
        const isLoggedIn = checkStatusLogin();
        //console.log(this.props.match.params.n_modulo)
        return (
            <Fragment>
                <Route 
                    render={() => 
                        isLoggedIn ? ( 
                            <ReactCSSTransitionGroup
                                component="div"
                                transitionName="TabsAnimation"
                                transitionAppear={true}
                                transitionAppearTimeout={0}
                                transitionEnter={false}
                                transitionLeave={false}>
                                <div className="app-inner-layout">                      
                                    <IndexZone n_modulo={this.props.match.params.n_modulo} />              
                                </div>
                            </ReactCSSTransitionGroup>
                                ) : (
                                    <Redirect to="/pages/login"></Redirect>
                                )
                        }      
                
                /> 
            </Fragment>
        )
    }
}
