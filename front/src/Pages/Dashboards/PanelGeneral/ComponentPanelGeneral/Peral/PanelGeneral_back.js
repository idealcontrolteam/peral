import React, {Component, Fragment} from 'react';

 import cx from 'classnames';
import {Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    CardFooter,
    Badge,ButtonGroup,ListGroupItem,ListGroup} from 'reactstrap';
import Loader from 'react-loaders';
import axios from 'axios';
import moment from 'moment';
import {BrowserRouter as Router,Switch,Route,Link, useRouteMatch} from "react-router-dom";
import ReactDOM from 'react-dom';
import {Redirect} from 'react-router';
import { API_ROOT} from '../../../../../api-config';
import IndexNave from '../../../Naves/IndexNave';
import BlockUi from 'react-block-ui';
import {
    toast

} from 'react-toastify';
import ReactTable from "react-table";
import {TagServices} from '../../../../../services/comun'
import * as _ from "lodash";
import '../../../../../assets/style.css'

let habilitado=false;

let sw=false;
let c_sw=false;
let c_sw2=false;
let c_sw3=false;
let visible="block";
let check=false;
let modificacion_general=false;
let array_gloabal=[];
let find_modulo=""

class PanelGeneralIgnao extends Component {  
    constructor(props) {
        super(props);
        this.state = {         
            isLoading:true,
            Tags: [],
            Ox:true,
            Temp:false,
            Sat:false,
            nCard:[1],
            Data:[],
            Modulos:[],
            mySalaModal:0,
            myModul:"",
            modal1: false,
            setpointOx:1,
            minOx:3,
            maxOx:0,
            timeOn:5,
            timeOff:0,
            histeresisOx:0,

            id_setpointOx:1,
            id_minOx:3,
            id_maxOx:0,
            id_timeOn:5,
            id_timeOff:0,
            id_histeresisOx:0,
            estadoTK:[],

            blocking1: false,  
            intentosGuardado: 0,
            dateTimeRefresh:"",
            locationId:"",
            zoneId:"",
            id_ConfigTag:"",
            find_modulo:"",
            arraySelected:[],
            Devices:[],
        }
        //this.checkTkChoice = this.checkTkChoice.bind(this);
        //this.refreshPage = this.refreshPage.bind(this);
        this.getButtonTag = this.getButtonTag.bind(this);
        this.tagservices = new TagServices();   
        this.timerRef=React.createRef();

    }
    
    
    componentDidMount = () => {
        //this.setState({ isLoading: false });    
        this.loadAllData();
        //this.intervalIdTag1 = setInterval(() => this.loadDataTags(),intervaloRefresco);
        let url = window.location.host;
        //let u=url.split('/');
        const hostname = window && window.location && window.location.hostname;
        if(hostname === 'ihc.idealcontrol.cl'){
            habilitado = true;
        }
    }
  
    componentWillUnmount = () => { 
          clearInterval(this.timerRef);  
    }

    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    loadAllData = () =>  {  
        this.tagservices.getLocation().then(locations=>{
            let array=locations.map((l)=>{ return{"locationId":l._id} })
            // console.log(array)
            array_gloabal=array
            this.loadDataTags()
            const intervaloRefresco = 30000;
            this.timerRef.current = setInterval(this.loadDataTags.bind(this),intervaloRefresco);
            this.setState({Data:locations})
        })
    }

    loadDataTags(){
        this.tagservices.getTagsArray(array_gloabal).then(tags=>{
            let fecha=this.reformatDate(tags[0].dateTimeLastValue)
            this.setState({
            Tags:tags,Devices:tags.map(t=>{
                let split=t.nameAddress.split('Z')
                return split[0]
            }).filter(this.onlyUnique),
            // Modulos:tags.map(l=>l.zoneId.code),
            dateTimeRefresh:fecha,isLoading: false})
        })
    }

    reformatDate(dateTime){
        return this.tagservices.reformatDate(dateTime)
    }
    
    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });   
        
        
        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            // console.log(response);
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );      
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                this.componentDidMount();
                 this.loadDataTag();
                 this.loadDataChar();
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
                //window.location.reload(false);
            }else if (this.state.intentosGuardado >= 10)
            {
                  this.componentDidMount();
                  //this.loadAllData();
                  //this.loadDataChar();
                  //toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  toast['success']('Almacenado Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false}); 
                  clearInterval(this.intervalConfirmacion);
                  //window.location.reload(false);
            }
        })
        .catch(error => {
          console.log(error);
        });

    }
  
    almacenarConfig = () => {
        let data=[
            "setpointOx",
            "minOx",
            "maxOx",
            "timeOn",
            "timeOff",
            "histeresisOx",
        ]
        let data2=[
            "set_point",
            "alarma_minima",
            "alarma_maxima",
            "time_on",
            "time_off",
            "histeresis",
        ]
        let {id_ConfigTag} = this.state;
        let conf={
            "setpoint": this.state[data[0]],
            "histeresis": this.state[data[5]],
            "timeOn": this.state[data[3]],
            "timeOff": this.state[data[4]],
            "active":true,
        }
        if(modificacion_general){
            this.setState({blocking1:true})
            data.map((key,i)=>{
                let obj={
                    "lastValue":this.state[key],
                    "lastValueWrite":this.state[key],
                    "write":true
                }
                this.state.Tags.filter(t=>t.zoneId==this.state.zoneId&&t.shortName.includes('oxd')).map(t=>{
                    this.tagservices.updateTagsConfig(t._id,conf).then(respuesta=>{
                        //alert(JSON.stringify(respuesta))
                    })
                })
                this.state.Tags.filter(t=>t.zoneId==this.state.zoneId&&t.shortName.includes(String(data2[i]))).map(t=>{
                    this.tagservices.updateTags(t._id,obj).then(respuesta=>{
                        if(respuesta.statusCode==201 || respuesta.statusCode==200){
                            //toast['success']('Se ha actualizado el '+String(data2[i]), { autoClose: 4000 })
                            this.setState({
                                Tags:this.state.Tags.filter(t2=>t2._id!=t._id)
                                .concat(this.state.Tags.filter(t2=>t2._id==t._id)
                                .map(t2=>{
                                    t2.lastValue=obj.lastValue
                                    return t2
                                })),blocking1:false
                            })
                        }
                    })
                })
                //Permite habilitar y deshabilitar modulo completo

                let id_zone=this.state.zoneId
                this.state.Tags.filter(l=>l.zoneId==id_zone).map(t=>{
                    let obj={
                        "state":sw,
                    }
                    this.tagservices.updateTags(t._id,obj).then(respuesta=>{
                        if(respuesta.statusCode==201 || respuesta.statusCode==200){
                            this.setState({
                                Tags:this.state.Tags.filter(t=>t.zoneId!=id_zone)
                                .concat(this.state.Tags.filter(t=>t.zoneId==id_zone)
                                .map(t=>{
                                    t.state=sw
                                    return t
                                })),
                            })
                        }
                        //toast['success']('Se ha actualizado el '+tag.name, { autoClose: 4000 })
                    })
                })
            })
        }else{
            this.tagservices.updateTagsConfig(id_ConfigTag,conf).then(respuesta=>{
                // alert(JSON.stringify(respuesta))
            })
            data.map(key=>{
                let obj={
                    "lastValue":this.state[key],
                    "lastValueWrite":this.state[key],
                    "write":true
                }
                this.tagservices.updateTags(this.state["id_"+key],obj).then(respuesta=>{
                    if(respuesta.statusCode==201 || respuesta.statusCode==200){
                        toast['success']('Se ha actualizado el '+key, { autoClose: 4000 })
                        this.setState({
                            Tags:this.state.Tags.filter(t=>t._id!=this.state["id_"+key])
                            .concat(this.state.Tags.filter(t=>t._id==this.state["id_"+key])
                            .map(t=>{
                                t.lastValue=obj.lastValue
                                return t
                            }))
                        })
                    }
                })
            })
            let id_location=this.state.locationId
            this.state.Tags.filter(l=>l.locationId==id_location).map(t=>{
                let obj={
                    "state":sw,
                }
                this.tagservices.updateTags(t._id,obj).then(respuesta=>{
                    if(respuesta.statusCode==201 || respuesta.statusCode==200){
                        this.setState({
                            Tags:this.state.Tags.filter(t=>t.locationId!=id_location)
                            .concat(this.state.Tags.filter(t=>t.locationId==id_location)
                            .map(t=>{
                                t.state=sw
                                return t
                            }))
                        })
                    }
                    //toast['success']('Se ha actualizado el '+tag.name, { autoClose: 4000 })
                })
            })
        }
    }

    toggleModal1(l,n) {
        if(n!=null){
            if(n.includes('-')){
                this.setState({["modal"+n.replace('-','')]:false})
            }else{
                this.setState({["modal"+n]:true})
            }
        }
        else if(l=="-1"){
            this.setState({
                modal1: !this.state.modal1,
            });
        }else{
            try{ 
                sw=this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('oxd'))[0].state;
                let name_modul=l.zoneId.name
                this.setState({
                    modal1: !this.state.modal1,
                    mySalaModal:l.name+" "+name_modul,
                    myModul:l.zoneId.name,
                    zoneId:l.zoneId._id,
                    locationId:l._id,
                    setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('set_point'))[0].lastValue,
                    maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('alarma_maxima'))[0].lastValue,
                    minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('alarma_minima'))[0].lastValue,
                    timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_on'))[0].lastValue,
                    timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_off'))[0].lastValue,
                    histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('histeresis'))[0].lastValue,
    
                    id_setpointOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('set_point'))[0]._id,
                    id_maxOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('alarma_maxima'))[0]._id,
                    id_minOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('alarma_minima'))[0]._id,
                    id_timeOn:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_on'))[0]._id,
                    id_timeOff:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('time_off'))[0]._id,
                    id_histeresisOx:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('histeresis'))[0]._id,
                    id_ConfigTag:this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes('oxd'))[0]._id,
                    //botones de control
    
                });
            }catch(e){}
        }
    }

    getManualAuto=(tag)=>{
        
    }

    getButtonTag=(array,n,num)=>{
        
    }

    handleClickGuardar=()=>{
            modificacion_general=false;
            this.almacenarConfig();
    }

    handleClickAplicar = () =>{
                modificacion_general=true;
                this.almacenarConfig();
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    cardTK(l,type,color){
        return this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes(type)).map((t,i)=>{
            return<Button id={l._id} style={{marginTop:13,marginLeft:10,backgroundColor:``}} 
                className={" mr-0 mb-1    pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast "+`${t.state==1?"":"opacity-6"}`} color={color} onClick={((e)=>{
                    this.toggleModal1(l);
                })}>     
                <div className="pl-1 size_label">{l.name}</div> 
                <span className="badge badge-light m-0 ml-0 pl-1 w-100 ">
                    <font size="2">{t.lastValue} <font size="1" color={"silver"}>{t.unity}</font></font>
                {/* <span className="opacity-6  pl-7 size_unidad2">  
                225 </span>  */}
                </span>
                <span  className={cx("badge badge-dot badge-dot-lg "+`${t.state==1?"badge-success":"badge-secondary"}`)}> </span>
            </Button>
        })
        
    }
    
    getColor(unidad){
        // #FC3939
        //1,,3,2
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            if(unidad=="bat"){
                return miscolores[10]
            }
            else if(unidad.toLowerCase().includes("oxd")){
                return miscolores[0]
            }else if(unidad.toLowerCase().includes("oxs")||unidad.toLowerCase().includes("sat")){
                return miscolores[2]
            }else if(unidad.toLowerCase().includes("temp")){
                return miscolores[1]
            }else if(unidad.toLowerCase().includes("sal")){
                return miscolores[3]
            }
            else{
                return ""
            }
    }

    getDataForDevice(code,typeData){
        let find_tag=this.state.Tags.filter(t=>t.nameAddress.includes(code))
        // console.log(find_tag)
        if(find_tag.length>0){
            return find_tag[0][typeData]
        }
    }

    cardModule(device,type){
        // console.log(this.getDataForDevice(device,'zoneId'))
        //let data=this.state.Data.filter(l=>l.zoneId._id==this.getDataForDevice(`Z${type}00`,'zoneId')
        let data=this.state.Tags.filter(t=>{t.nameAddress.includes(device)}
        // &&!l.name.includes('Cabecera')
        // &&!l.name.includes('Filtro Uv')
        // &&!l.name.includes('Sala Incubacion')
        )
        // data=_.orderBy(data.map(d=>{
        //     d.order=this.getLocationForId(d.locationId,'order')
        //     return  d
        // }),["order"],["asc"])
        console.log(data)
        return<Card>
            <CardBody>
                {/* <CardTitle style={{textAlign: 'center'}}></CardTitle> */}
                <p style={{textAlign: 'right'}}>{device}</p>
                <hr></hr>
                <Row style={{width:`${100}%`}}>
                    {
                        // data.map((l,i)=>{
                        this.getTypeButton(data,null,type)
                        // type==1?
                        //     (<Col style={{width:`${100/7}%`}}>{this.getTypeButton(data,type)}</Col>):
                        //     (<Col style={{width:`${100/5}%`}}>{this.getTypeButton(data,type)}</Col>)
                        // })
                    }
                </Row>
            </CardBody>
        </Card>
    }

    typeAlarmColor(tag){
        if(!this.state.Temp && !this.state.Sat){
            try{
                let maxOx=this.state.Tags.filter(t=>t.locationId==tag.locationId&&t.shortName.toLowerCase()('BANDA_MAX'))[0].lastValue
                let minOx=this.state.Tags.filter(t=>t.locationId==tag.locationId&&t.shortName.toLowerCase()('BANDA_MIN'))[0].lastValue
                if(tag.lastValue>maxOx){
                   return "badge-danger"
                }else if(tag.lastValue<minOx){
                   return "badge-danger"
                }else if(tag.state==0){
                   return "badge-secondary"
                }else{
                   return "badge-success"
                }
            }catch(e){
                return "badge-warning"
            }
        }
    }

    // getLocationForId=(locationId,typeData)=>{
    //     let find_tag=this.state.Data.filter(l=>l._id==locationId)
    //     if(find_tag.length>0){
    //         return typeData!=null?
    //         find_tag[0][typeData]:
    //         find_tag[0]
    //     }
    // }

    getNewButton(tags,type,color,type_module){
        //let t=this.state.Tags.filter(t=>t.locationId==l._id&&t.shortName.toLowerCase().includes(type))[0]
        let name=""
        console.log(tags)
        if(tags){
            tags=tags.filter(t=>t.shortName.toLowerCase()==type)
            let ancho=window.innerWidth<=1240?51:60

            return tags.map(t=>{
                name=t.nameAddress.split('TK')
                console.log(t)
                return t&&<Col style={{width:`${type_module==2?100/7:100/5}%`}}><Button style={{backgroundColor:``,borderRadius:`${50}%`,width:ancho,height:ancho,boxShadow:"5px 5px 5px #888"}} 
                    className={" mr-0 mb-1    pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast "+`${t.state==1?"":"opacity-6"}`} 
                    color={color} onClick={((e)=>{
                        // let location=this.getLocationForId(t.locationId)
                        // console.log(location)
                        // this.toggleModal1(location);
                    })}>     
                    <div className="pl-1 size_label" style={{fontSize:10,marginLeft:3}}>
                        {("TK"+parseInt(name[1])).replace("T"+type.toUpperCase(),'')}
                    </div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100 " style={{borderRadius:10}}>
                        <font size="2">{t.lastValue} <font size="1" color={"silver"}>{t.unity}</font></font>
                    {/* <span className="opacity-6  pl-7 size_unidad2">  
                    225 </span>  */}
                    </span>
                    {/* {cabeceras.length==0&&(<span  className={cx("badge badge-dot badge-dot-lg "+`${this.typeAlarmColor(t)}`)}> </span>)} */}
                </Button></Col>
            })
           
        }
        
        // let cabeceras=[t].filter(tag=>tag.name.includes('Cabecera')
        //                         ||tag.name.includes('filtro uv')
        //                         ||tag.name.includes('sala incub'))
        
        
    }

    getTypeButton(l,color,type_module){
        let {Ox,Temp,Sat}=this.state;
        // let l=""
        if(color!=null){
            if(color.includes("oxd")){
                return this.getNewButton(l,"oxd","primary",type_module)
            }
            if(color.includes("temp")){
                return this.getNewButton(l,"temp","danger",type_module)
            }
            if(color.includes("oxs")){
                return this.getNewButton(l,"oxs","warning",type_module)
            }
            if(color.includes("ph")){
                return this.getNewButton(l,"ph","success",type_module)
            }
        }else{
            if(Ox){
                return this.getNewButton(l,"oxd","primary",type_module)
            }
            if(Temp){
                return this.getNewButton(l,"temp","danger",type_module)
            }
            if(Sat){
                return this.getNewButton(l,"oxs","warning",type_module)
            }
        }
    }

    // getNewOrderStatic(){
    //     let data=this.state.Modulos.filter(this.onlyUnique)
    //     let m100=d=>d.includes('100');
    //     let m200=d=>d.includes('200');
    //     let m300=d=>d.includes('300');
    //     let m300_chico=d=>d.includes('400');
    //     console.log(this.state.Data.filter(l=>l.zoneId.name.includes(data.filter(m300))))
    //     console.log(this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m200))))
    //     if(this.state.Modulos.length>0){
    //         return<Row>
    //         <Col md={4}>
    //             <Card>
    //             <CardTitle>{data.filter(m300)}</CardTitle>
    //             <Row>
    //                 {
    //                     this.state.Data.filter(l=>l.zoneId.name.includes(data.filter(m300))
    //                     &&!l.name.includes('Cabecera')
    //                     &&!l.name.includes('Filtro Uv')
    //                     &&!l.name.includes('Sala Incubacion')
    //                     ).map((l,i)=>{
    //                         return <Col  xs="3" sm="3" md="3">{this.getTypeButton(l)}</Col>
    //                     })
    //                 }
    //             </Row>
    //             </Card>
    //         </Col>
    //         <Col md={3}>
    //             <Card>
    //             <CardTitle>{data.filter(m300_chico).map(d=>"Modulo 300 chico")}</CardTitle>
    //             <Row>
    //                 {
    //                     this.state.Data.filter(l=>l.zoneId.name.includes(data.filter(m300_chico))).map((l,i)=>{
    //                         return <Col xs="4" sm="4" md="4">{this.getTypeButton(l)}</Col>
    //                     })
    //                 }
    //             </Row>
    //             </Card>
    //             <Row>
    //                 <Col md={12} style={{height:85}}></Col>
    //                 <Col md={12} style={{height:85}}></Col>
    //             </Row>
    //             <Card>
    //             <Row>
    //                  {
    //                      this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m200))&&l.name=="TK219"||l.name=="TK220").map((l,i)=>{
    //                         return <Col xs="6" sm="6" md="6">{this.getTypeButton(l)}</Col>
    //                     })
    //                  }
    //             </Row>
    //             </Card>
    //         </Col>
    //         <Col md={5}>
    //             <Row>
    //                 <Col md={12} style={{height:85}}></Col>
    //             </Row>
    //             <Card>
    //             <CardTitle>{data.filter(m100)}</CardTitle>
    //             <Row>
    //                 {
    //                      this.state.Data.filter(l=>l.zoneId.name.includes(data.filter(m100))
    //                      &&!l.name.includes('Cabecera')
    //                     &&!l.name.includes('Filtro Uv')
    //                     &&!l.name.includes('Sala Incubacion')
    //                     ).map((l,i)=>{
    //                         return <Col xs="2" sm="2" md="2">{this.getTypeButton(l)}</Col>
    //                     })
    //                 }
    //             </Row>
    //             </Card>
    //             <Row>
    //                 <Col md={12} style={{height:85}}></Col>
    //             </Row>
    //             <Card>
    //                 <Row>
    //                 {
    //                     this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m200))&&l.name!="TK219"&&l.name!="TK220"
    //                     &&!l.name.includes('Cabecera')
    //                     &&!l.name.includes('Filtro Uv')
    //                     &&!l.name.includes('Sala Incubacion')).map((l,i)=>{
    //                         return <Col xs="2" sm="2" md="2">{this.getTypeButton(l)}</Col>
    //                     })
    //                 }
    //                 </Row>
    //                 <hr></hr>
    //                 <CardTitle>{data.filter(m200)}</CardTitle>
    //             </Card>
    //         </Col>
    //     </Row>
    //     }else{
    //         return ""
    //     }
    // }

    getCabeceras(){
        let data=this.state.Modulos.filter(this.onlyUnique)
        // console.log(data)
        let m100=d=>d.includes('100');
        let m200=d=>d.includes('200');
        let m300=d=>d.includes('300');
        return<Fragment>
            {/* <Col md={4}>
            
            </Col>
            <Col md={4}>
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m200))&&l.name.includes('Cabecera')).map((l,i)=>{
                    return <Col xs="2" sm="2" md="2">{this.getTypeButton(l)}</Col>
                })
            }
            </Col>
            <Col md={4}>
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m300))&&l.name.includes('Cabecera')).map((l,i)=>{
                    return <Col xs="2" sm="2" md="2">{this.getTypeButton(l)}</Col>
                })
            }
            </Col> */}
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m100))
                &&l.name.includes('Cabecera')
                ).map((l,i)=>{
                    return <CardHeader style={{height:"auto"}}>
                        <Row style={{width:`${100}%`,height:`${100}%`}}>
                            <Col xs="12" sm="4" md="4">
                                <Row style={{ marginTop:-5,marginLeft:10}}>
                                    <CardTitle style={{marginTop:`${5}%`}}>{l.name}</CardTitle>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxd")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"temp")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxs")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"ph")}</Col>
                                </Row>
                            </Col>
                        </Row>
                    </CardHeader>
                })
            }
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m100))
                &&l.name.includes('Filtro Uv')
                ).map((l,i)=>{
                    return <CardHeader style={{height:"auto"}}>
                        <Row style={{width:`${100}%`,height:`${100}%`}}>
                            <Col xs="12" sm="4" md="4">
                                <Row style={{ marginTop:-5,marginLeft:10}}>
                                    <Col xs="3" sm="3" md="3"><CardTitle style={{marginTop:`${10}%`}}>{"Filitro UV"}</CardTitle></Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxd Filtro Uv")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"temp Filtro Uv")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"sat Filtro Uv")}</Col>
                                </Row>
                            </Col>
                        </Row>
                    </CardHeader>
                })
            }
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m100))
                &&l.name.includes('Sala Incubacion')
                ).map((l,i)=>{
                    return <CardHeader style={{height:"auto"}}>
                        <Row style={{width:`${100}%`,height:`${100}%`}}>
                            <Col xs="12" sm="4" md="4">
                                <Row style={{ marginTop:-5,marginLeft:10}}>
                                    <Col xs="3" sm="4" md="4"><CardTitle style={{marginTop:`${10}%`}}>{"Estanque Incubación"}</CardTitle></Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxd Sala Incubacion")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"temp Sala Incubacion")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"sat Sala Incubacion")}</Col>
                                </Row>
                            </Col>
                        </Row>
                    </CardHeader>
                })
            }
            {/* {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m200))&&l.name.includes('Cabecera')).map((l,i)=>{
                    return <CardHeader style={{height:"auto"}}>
                        {l.name}
                        <Row style={{width:`${100}%`,height:`${100}%`}}>
                            <Col xs="12" sm="6" md="6">
                                <Row style={{ marginTop:-5,marginLeft:10}}>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxd")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"temp")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"sat")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"ph")}</Col>
                                </Row>
                            </Col>
                            <Col xs="12" sm="6" md="6">
                            </Col>
                        </Row>
                    </CardHeader>
                })
            }
            {
                this.state.Data.filter((l,i)=>l.zoneId.name.includes(data.filter(m300))&&l.name.includes('Cabecera')).map((l,i)=>{
                    return <CardHeader style={{height:"auto"}}>
                        {l.name}
                        <Row style={{width:`${100}%`,height:`${100}%`}}>
                            <Col xs="12" sm="6" md="6">
                                <Row style={{ marginTop:-5,marginLeft:10}}>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"oxd")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"temp")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"sat")}</Col>
                                    <Col xs="3" sm="2" md="2">{this.getTypeButton(l,"ph")}</Col>
                                </Row>
                            </Col>
                            <Col xs="12" sm="6" md="6">
                            </Col>
                        </Row>
                    </CardHeader>
                })
            } */}
        </Fragment>
    }

    configuracionPanel(m){
        let data=this.state.Data
        let filtro=d=>d.zoneId.name.includes(m)
        let module=this.state.Modulos.filter(this.onlyUnique)
        return<Row style={{marginTop:10}}>     
            <Col md="6">
                <Row>
                    <Col xs="4" sm="4" md="4">
                        <Card>
                            <CardTitle>{m}</CardTitle>
                            <Row style={{marginLeft:`${15}%`}}>
                                {
                                    data.filter(filtro).map((l,i)=>{
                                        //console.log(l._id)
                                        return <Col xs="6" sm="6" md="6">
                                            <Button color={l.select==null?"secondary":"primary"} 
                                            style={{with:"auto",height:"auto",marginTop:5}} onClick={(()=>{
                                                l.select=null
                                                this.setState({Data:this.state.Data.filter(d=>d._id!=l._id).concat(l)})
                                            })}>
                                                {l.select==null?"TK":l.name}
                                            </Button>
                                        </Col>
                                    })
                                }
                                <Col style={{height:15}}></Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                {/* <Row>
                    <Col md={3}><Button style={{marginTop:2}} color={"secondary"}>tk</Button></Col>
                </Row> */}
            </Col>
            <Col md="1"></Col> 
            <Col md="5">
                <ListGroup>
                    {
                        data.filter(filtro).map((l,i)=>{
                            //console.log(l._id)
                            // console.log(l)
                            return <ListGroupItem style={{cursor:"pointer",display:`${l.select==null?"block":"none"}`}} onClick={(()=>{
                                    l.select=true
                                    this.setState({
                                        Data:this.state.Data.filter(d=>d._id!=l._id).concat(l),
                                        // arraySelected:this.state.arraySelected.concat({
                                            
                                        // })
                                    })
                                })}>
                                {l.name} - {l.zoneId.name}
                            </ListGroupItem>
                        })
                    }
                </ListGroup>
            </Col>
        </Row>
    }

    // asignPosition=(name)=>{
    //     let posicion=0
    //     if(parseInt(name[name.length-1])===1){
    //         posicion=2
    //     }else if(parseInt(name[name.length-1])===2){
    //         posicion=1
    //     }else if(parseInt(name[name.length-1])===3){
    //         posicion=4
    //     }else if(parseInt(name[name.length-1])===4){
    //         posicion=3
    //     }
    //     console.log(name[name.length-1])
    //     console.log(posicion)
    //     return posicion
    // }
    viewDivices=(posicion,type)=>{
        //let dispositivos=orden==1?this.state.Devices:this.state.Devices.reverse()
        return this.state.Devices.map((d,i)=>{
            // console.log(m)
            // return <Col xs="12" sm="6" md="6">
            //         {this.cardModule2(m)}
            //     </Col>
            if(i==posicion && window.innerWidth>1400){
                return <Col xs="12" sm="6" md="6">
                    {this.cardModule(d,type)}
                </Col>
            }else if(i==posicion){
                return <Col xs="12" sm="12" md="12">
                    {this.cardModule(d,type)}
                </Col>
            }
        })
    }
  
    render() { 
        if (this.state.isLoading) {
            return <Loader type="ball-pulse"/>;
        }   

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        const testvalue = "hello";
        let filtro_mod=m=>m==this.state.find_modulo
        let posiciones=[]
    return (

        
        <Fragment>

            <Modal isOpen={this.state.modal1}>
                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion {this.state.mySalaModal}</ModalHeader>
                    <ModalBody>

                    <Row>     
                    
                        <Col  xs="12" md="12" lg="12">
                            <Card className="main-card mb-3">
                                <CardBody>
                                    <CardTitle>Oxigeno</CardTitle>
                                    <Form>
                                        <Row>

                                            <Col xs="6" md="6" lg="6">
                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                <Label for="setpointOx" className="m-0">Set Point</Label>
                                                <InputGroup>                                                                                      
                                                    <Input   id="setpointOx" defaultValue={this.state.setpointOx}  onChange={e => this.inputChangeHandler(e)}/>
                                                    <InputGroupAddon addonType="append">                                                                                  
                                                    <InputGroupText>mg/L</InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </FormGroup>       
                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                    <Label for="timeOn" className="m-0">Time On</Label>
                                                    <InputGroup>                                                                                      
                                                        <Input  id="timeOn" defaultValue={this.state.timeOn}   onChange={e => this.inputChangeHandler(e)}/>
                                                        <InputGroupAddon addonType="append">                                                                                  
                                                        <InputGroupText>Min</InputGroupText>
                                                        </InputGroupAddon>
                                                    </InputGroup>                                                                                 
                                                </FormGroup>                                                                    
                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                <Label for="maxOx" className="m-0">Alarma Alta de Oxígeno</Label>
                                                <InputGroup>                                                                                      
                                                    <Input   id="maxOx" defaultValue={this.state.maxOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                    <InputGroupAddon addonType="append">                                                                                  
                                                    <InputGroupText>mg/L</InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>                                                                                 
                                            </FormGroup>
                                            </Col>

                                            <Col xs="6" md="6" lg="6">
                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                    <Label for="histeresisOx" className="m-0">Histeresis</Label>
                                                    <InputGroup>                                                                                      
                                                        <Input   id="histeresisOx" defaultValue={this.state.histeresisOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                        <InputGroupAddon addonType="append">                                                                                  
                                                        <InputGroupText>mg/L</InputGroupText>
                                                        </InputGroupAddon>
                                                    </InputGroup>                                                                                 
                                                </FormGroup>
                                                <FormGroup   className="mt-2" style={{display:visible}}>
                                                    <Label for="timeOff" className="m-0">Time Off</Label>
                                                    <InputGroup >                                                                                      
                                                        <Input  id="timeOff" defaultValue={this.state.timeOff}   onChange={e => this.inputChangeHandler(e)}/>
                                                        <InputGroupAddon addonType="append">                                                                                  
                                                        <InputGroupText>Min</InputGroupText>
                                                        </InputGroupAddon>
                                                    </InputGroup>                                                                                 
                                                </FormGroup>
                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                    <Label for="minOx" className="m-0">Alarma Baja de Oxígeno</Label>
                                                    <InputGroup>                                                                                      
                                                        <Input   id="minOx" defaultValue={this.state.minOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                        <InputGroupAddon addonType="append">                                                                                  
                                                        <InputGroupText>mg/L</InputGroupText>
                                                        </InputGroupAddon>
                                                    </InputGroup>                                                                                 
                                                </FormGroup>
                                            </Col>

                                        </Row>
                                    </Form>
                                </CardBody>
                            </Card>

                            <Card>
                                <CardBody>
                                <FormGroup   className="mt-2" >
                                    <Row>
                                        <Col xs="6" md="6" lg="6">
                                            <CardTitle>Estado</CardTitle>
                                        </Col>
                                        <Col xs="6" md="6" lg="6">
                                            <InputGroup style={{paddingTop:5}}>
                                                {
                                                    this.state.nCard.map((c,i)=>{
                                                        if(i==0)
                                                            if(sw){
                                                                return <label key={i} className="switch">
                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                // this.setState({SW:false})
                                                                                sw=false;
                                                                            })} onChange={e => this.inputChangeHandler(e)} />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                            }else{
                                                                return <label key={i} className="switch">
                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                // this.setState({SW:true})
                                                                                sw=true;
                                                                            })} onChange={e => this.inputChangeHandler(e)}/>
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                            }
                                                    })
                                                }  
                                                <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Habilitar TK</Label>
                                                </InputGroup>
                                        </Col>
                                    </Row>
                                    <hr />
                                    {/* <Row>
                                        <Col xs="6" md="6" lg="6">
                                            <CardTitle>Control</CardTitle>
                                        </Col>
                                        <Col xs="6" md="6" lg="6">
                                                <InputGroup style={{paddingTop:5}}>
                                                    {
                                                        this.state.nCard.map((c,i)=>{
                                                            if(i==1)
                                                                if(c_sw){
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK2" checked={c_sw} onClick={(()=>{
                                                                                    // this.setState({SW:false})
                                                                                    c_sw=false;
                                                                                    c_sw3=true;
                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }else{
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK2" checked={c_sw} onClick={(()=>{
                                                                                    // this.setState({SW:true})
                                                                                    c_sw=true;
                                                                                    c_sw2=false;
                                                                                    c_sw3=false;
                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }
                                                        })
                                                    }  
                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Automatico</Label>
                                                    </InputGroup>
                                                    
                                                    <InputGroup style={{paddingTop:5}}>
                                                    {
                                                        this.state.nCard.map((c,i)=>{
                                                            if(i==1)
                                                                if(c_sw2){
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK3" checked={c_sw2} onClick={(()=>{
                                                                                    // this.setState({SW:false})
                                                                                    c_sw2=false;
                                                                                    c_sw=true;
                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }else{
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK3" checked={c_sw2} onClick={(()=>{
                                                                                    // this.setState({SW:true})
                                                                                    c_sw2=true;
                                                                                    c_sw=false;
                                                                                    c_sw3=false;
                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }
                                                        })
                                                    }  
                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual On</Label>
                                                    </InputGroup>      

                                                    <InputGroup style={{paddingTop:5}}>
                                                    {
                                                        this.state.nCard.map((c,i)=>{
                                                            if(i==1)
                                                                if(c_sw3){
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK4" checked={c_sw3} onClick={(()=>{
                                                                                    // this.setState({SW:false})
                                                                                    c_sw3=false;
                                                                                    c_sw=true;
                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }else{
                                                                    return <label key={i} className="switch">
                                                                                <input type="checkbox" id="estadoTK4" checked={c_sw3} onClick={(()=>{
                                                                                    // this.setState({SW:true})
                                                                                    c_sw3=true;
                                                                                    c_sw2=false;
                                                                                    c_sw=false;
                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                }
                                                        })
                                                    }  
                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual Off</Label>
                                                    </InputGroup>
                                            </Col>
                                        </Row> */}
                                        
                                        </FormGroup>
                                </CardBody>
                            </Card>
                        </Col>
                        
                    </Row>
                    
                    </ModalBody>
                    <ModalFooter>
                        <Row style={{width:`${100}%`}}>
                                <Col xs="6" md="6" lg="6"><Button color="primary" disabled={habilitado} onClick={() =>this.handleClickAplicar()}>Aplicar a {`${this.state.myModul.includes('400')?"300 Chico":this.state.myModul}`}</Button></Col>
                                <Col xs="3" md="3" lg="3"><Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button></Col>
                                <Col xs="3" md="3" lg="3"><Button color="primary" disabled={habilitado} onClick={() => this.handleClickGuardar()}>Guardar</Button>{' '}</Col>
                            </Row>
                    </ModalFooter>
                    </BlockUi>
            </Modal>
            <Modal isOpen={this.state.modal2} style={{maxHeight:660,maxWidth:`${80}%`}}>
                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                    <ModalHeader toggle={() => this.toggleModal1("","-2")}>Configuración Panel General</ModalHeader>
                    <ModalBody>
                        <Row>
                            {
                                this.state.Modulos.filter(this.onlyUnique).map(m=>{
                                    return <Col md={2}><Button style={{width:`${100}%`}} color={"primary"} outline onClick={(()=>{
                                        this.setState({find_modulo:m})
                                    })}>{m}</Button></Col>
                                })
                            }
                        </Row>
                        {
                            this.state.Modulos.filter(filtro_mod).map((m,i)=>{
                                if (i==0){
                                    return this.configuracionPanel(m)
                                }
                            })
                        }
                    </ModalBody>
                    <ModalFooter>
                        <Row style={{width:`${100}%`}}>
                                <Col xs="8" md="8" lg="8"></Col>
                                <Col xs="2" md="2" lg="2"><Button color="link" onClick={() => this.toggleModal1("","-2")}>Cancel</Button></Col>
                                <Col xs="2" md="2" lg="2"><Button color="primary" disabled={habilitado} onClick={() => this.handleClickGuardar()}>Guardar</Button>{' '}</Col>
                            </Row>
                    </ModalFooter>
                    </BlockUi>
            </Modal>
                            
            <Row>

                {/* {console.log(this.state.Data)}
                {console.log(this.state.Modulos.filter(this.onlyUnique))}
                {console.log(this.state.Tags)} */}
                {/* {console.log(sw)}
                {console.log(c_sw)}
                {console.log(c_sw2)}
                {console.log(c_sw3)} */}
                <Col md="12" lg="12">

                    <Card className="main-card mb-1">

                        <div class="body-block-example-3 d-none">
                            <div class="loader">
                                <div class="line-scale-pulse-out">
                                    <div class="bg-warning"></div>
                                    <div class="bg-warning"></div>
                                    <div class="bg-warning"></div>
                                    <div class="bg-warning"></div>
                                    <div class="bg-warning"></div>
                                </div>
                            </div>
                        </div>
                                                
                        <CardHeader className="card-header-tab" style={{height:50}}>
                                <div className="card-header-title font-size-lg text-capitalize font-weight-normal" style={{marginLeft:-20}}>
                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                            <Button 
                                            className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Ox?"":"#545cd8"}`}} outline color={miscolores[0]}
                                            onClick={()=>{
                                                this.setState({
                                                    Ox:true,
                                                    Temp:false,
                                                    Sat:false
                                                })
                                            }}>  
                                                {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                <span className="opacity-6  pl-0 size_unidad">  </span>
                                                </div>
                                                <div className="widget-subheading">
                                                    {/* {data.shortName} opacity-1 */}<font color={!this.state.Ox?"":"#fff"}>Ox (mg/L)</font>
                                                    </div>
                                            </Button>  
                                    </div>
                                    
                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                            <Button 
                                            className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Temp?"":"#545cd8"}`}} outline color={miscolores[0]}
                                            onClick={()=>{
                                                this.setState({
                                                    Ox:false,
                                                    Temp:true,
                                                    Sat:false
                                                })
                                            }}>  
                                                {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                <span className="opacity-6  pl-0 size_unidad">  </span>
                                                </div>
                                                <div className="widget-subheading">
                                                    {/* {data.shortName} */}<font color={!this.state.Temp?"":"#fff"}>Temp (°C)</font>
                                                    </div>
                                            </Button>  
                                    </div>
                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                            <Button 
                                            className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Sat?"":"#545cd8"}`}} outline color={miscolores[0]}
                                            onClick={()=>{
                                                this.setState({
                                                    Ox:false,
                                                    Temp:false,
                                                    Sat:true
                                                })
                                            }}>  
                                                {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                <span className="opacity-6  pl-0 size_unidad">  </span>
                                                </div>
                                                <div className="widget-subheading">
                                                    {/* {data.shortName} */}<font color={!this.state.Sat?"":"#fff"}>Sat (%)</font>
                                                    </div>
                                            </Button>  
                                    </div>
                                    {/* <div id="rootB1"/>
                                    <div id="rootB2"/>
                                    <div id="rootB3"/>
                                    <div id="rootB4"/>
                                    <div id="rootB5"/>
                                    <div id="rootB6"/>
                                    <div id="rootB7"/>
                                    <div id="rootB8"/> */}  
                                    <Button color={"primary"} style={{right:1,position:"absolute",display:"none"}} 
                                    onClick={(()=>this.toggleModal1("","2"))}>Editar <i class="pe-7s-pen"> </i></Button>   
                                </div>
                                   
                        </CardHeader>
                        <CardBody className="p-0 opacity-0"> 
                            <Row>
                                <Col xs="12" sm="6" md="7">
                                    <Row className="no-gutters">
                                        {this.viewDivices(6,2)}
                                        {this.viewDivices(7,2)}

                                        {this.viewDivices(4,2)}
                                        {this.viewDivices(5,2)}
                                    </Row>
                                    <Card>
                                        <CardTitle>Sala de Alimentación</CardTitle>
                                    </Card>
                                </Col>
                                <Col xs="12" sm="6" md="5">
                                    <Row className="no-gutters">
                                        {this.viewDivices(1,1)}
                                        {this.viewDivices(0,1)}
                                        
                                        {this.viewDivices(3,1)}
                                        {this.viewDivices(2,1)}
                                        {/* {
                                            this.state.Devices.filter(this.onlyUnique).map((m,i)=>{
                                                if(i<4 && window.innerWidth>1400){
                                                    return <Col xs="12" sm="6" md="6">
                                                        {this.cardModule1(m)}
                                                    </Col>
                                                }else if(i<4){
                                                    return <Col xs="12" sm="12" md="12">
                                                        {this.cardModule1(m)}
                                                    </Col>
                                                }
                                            })
                                        } */}
                                    </Row>
                                    <Card>
                                        <CardTitle>Sala de Alevines</CardTitle>
                                    </Card>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter className="text-muted center">
                            <Row>
                                <Col size={6}>
                                    Última Actualización: {this.state.dateTimeRefresh!=""?moment(this.state.dateTimeRefresh).format('DD-MM-YYYY HH:mm:ss'):""}
                                </Col>
                                <div className="badge badge-dot badge-dot-lg badge-danger">Danger</div><p style={{marginLeft:4,marginRight: 4}}>Alarmado | </p>
                                <div className="badge badge-dot badge-dot-lg badge-success">Success</div><p style={{marginLeft:4,marginRight: 4}}>Normalizado | </p>
                            </Row>
                        </CardFooter>
                        </Card>
                        <Card>
                            {/* {this.getCabeceras()} */}
                        </Card>
                </Col>

                {/* ":"rgb(84, 92, 216)"} */}
               
            </Row>
      </Fragment>
    )
  }

}

export default PanelGeneralIgnao;