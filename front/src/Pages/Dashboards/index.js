import React, {Fragment} from 'react';
import {Route} from 'react-router-dom';

// DASHBOARDS
import PanelGeneral from './PanelGeneral/IndexPanelGeneral';
// import IndexZona1 from './Zonas/IndexZona1';
//import IndexZona2 from './Zonas/IndexZona2';
import IndexNave from './Naves/IndexNave';
import IndexNave2 from './Naves/IndexNave2';
import IndexNave3 from './Naves/IndexNave3';
import IndexNave4 from './Naves/IndexNave4';

import IndexNave5 from './Naves/IndexNave5';
import IndexNave6 from './Naves/IndexNave6';
import IndexNave7 from './Naves/IndexNave7';
import IndexNave8 from './Naves/IndexNave8';
import Tendencia from './Tendencia/IndexTendencia';
import tagservices from '../../services/comun'
// import HistAlarmas from './HistAlarmas/IndexHisAlarmas';
// import HistFallas from './HistFallas/IndexHisFallas';

// Layout
import AppHeader from '../../Layout/AppHeader';
import AppSidebar from '../../Layout/AppSidebar';

// Theme Options
import ThemeOptions from '../../Layout/ThemeOptions';

const Dashboards = ({match}) => (
    <Fragment>
        <ThemeOptions/>
        <AppHeader/>
        <div className="app-main">
            <AppSidebar/>
            <div className="app-main__outer">
                <div className="app-main__inner">
                    <Route path={`${match.url}/panelgeneral`} component={PanelGeneral}/>
                    {/* <Route path={`${match.url}/zona1`} component={IndexZona1}/> */}
                    {/* <Route path={`${match.url}/zona2`} component={IndexZona2}/> */}
                    <Route path={`${match.url}/tendencia`} component={Tendencia}/>
                    <Route path={`${match.url}/modulo/:n_modulo`} component={IndexNave}/>
                    {/* <Route path={`${match.url}/modulo/2`} component={IndexNave2}/>
                    <Route path={`${match.url}/modulo/3`} component={IndexNave3}/>
                    <Route path={`${match.url}/modulo/4`} component={IndexNave4}/>
                    <Route path={`${match.url}/modulo/5`} component={IndexNave5}/>
                    <Route path={`${match.url}/modulo/6`} component={IndexNave6}/>
                    <Route path={`${match.url}/modulo/7`} component={IndexNave7}/>
                    <Route path={`${match.url}/modulo/8`} component={IndexNave8}/> */}
                    {/* <Route path={`${match.url}/histalarmas`} component={HistAlarmas}/>
                    <Route path={`${match.url}/histfallas`} component={HistFallas}/> */}
                </div>           
            </div>
        </div>
    </Fragment>
);

export default Dashboards;