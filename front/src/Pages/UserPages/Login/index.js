import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';
import {Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import backgroundImage from '../../../assets/utils/images/sidebar/abstract2.jpg';  
import city3 from '../../../assets/utils/images/dropdown-header/headerbg1.jpg';
import {connect} from 'react-redux';
import {
    setNameUsuario,setEmailUsuario,setRolUsuario
} from '../../../reducers/Session';
// import CryptoJS from 'crypto-js';
//import sha256 from 'crypto-js/sha256';
// Layout
import { API_ROOT } from '../../../api-config';
import axios from "axios";

const app="rupanquito";

const APILogin= `${API_ROOT}/user/login`;


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
                email: "",
                password: "",
                status: "",
                message: ""
        };
         this.ingresarNameUsuario = this.ingresarNameUsuario.bind(this);
         this.ingresarRolUsuario = this.ingresarRolUsuario.bind(this);
        
    }
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.handleLogin();
        }
    }
    handleChange = event => {
            const name = event.target.name;
            const value = event.target.value;
            this.setState({ [name]: value });
    };

    ingresarNameUsuario = (usuario) => {
        let {setNameUsuario} = this.props;
        setNameUsuario(usuario);
    }
    ingresarEmailUsuario = (email) => {
        let {setEmailUsuario} = this.props;
        setEmailUsuario(email);
    }
    ingresarRolUsuario = (rol) => {
        let {setRolUsuario} = this.props;
        setRolUsuario(rol);
    }

    
    handleLogin = () => {
        const data = {
        email: this.state.email,
        password: this.state.password
        };

        axios
        .post(APILogin, data)
                .then(response => {   
                if (response.data.statusCode === 200) { 
                    console.log(response.data.data.rol)
                    //sessionStorage.setItem("rol", response.data.data.rol);
                   // console.log(response.data.data.user);
                //     const encrypted = this.state.email;
                //  //   console.log(encrypted);  
                //     const ciphertext = CryptoJS.AES.encrypt (encrypted,  'secret' );            
                //     //console.log(usuario);
                //     this.ingresarNameUsuario(ciphertext.toString());
                //     this.setState({ isloggedIn: true });

                   // console.log(this.state.email)
                    
                       const user = response.data.data.user;      
                       
                       localStorage.setItem("token"+app,response.data.data.token);
                       console.log(response.data.data)
                                
                       this.ingresarEmailUsuario(this.state.email);
                       this.ingresarRolUsuario(response.data.data.rol);
                       this.ingresarNameUsuario(user);
                       this.setState({ isloggedIn: true });
                  
                } else {
                    this.setState({ isloggedIn: false });   
                }
                })
                .catch(error => {
                      
                        this.setState({ isloggedIn: false });
                         this.setState({ status: "error", message: "Credenciales no válidas" });
                });
     };

  
    render()
     {
        
        let message = "";


        const errorStyle = {
          backgroundColor: "#f5d3d3",
          color: "black",
          textAlign: "center",
          borderRadius: "3px",
          border: "1px solid darkred",
          marginBottom: "15px",
          padding: "5px",
          fontSize: "15px",
          width: "100%"
        };
    
        // if (this.state.isloggedIn) {
        //     return <Redirect to="/dashboards/panelgeneral"></Redirect>
        // }
        if (this.state.isloggedIn) {
            return <Redirect to="/dashboards/panelgeneral"></Redirect>
        }

        if (this.state.status === "error") {
            message = <div style={errorStyle}>{this.state.message}</div>;
        }
        return (

    <Fragment>

        <div className="h-100 bg-heavy-rain bg-animation opacity-11" >  
            <div className="menu-header-image h-100 opacity-9"
                style={{
                    backgroundImage: 'url(' + city3 + ')'
                }}
            >                          
                            
            <div className="d-flex h-100 justify-content-center align-items-center">
                <Col md="8" className="mx-auto app-login-box">
                    <div className="modal-dialog w-100 mx-auto">
                        <div className="modal-content">
                            <div className="modal-body">
                               <div className="app-logo mx-auto mb-3 mt-4"/>
                                <div className="h5 modal-title text-center">
                                    <h4 className="mt-2">
                                        
                                        <div>Bienvenido</div>
                                        <span>Ingrese credenciales de acceso</span>
                                    </h4>
                                </div>
                                <Form>
                                    <Row form>
                                       {message}
                                        <Col md={12}>
                                        <FormGroup className="mt-2">
                                                    <Label for="exampleEmail" className="m-0">Email</Label>
                                                    <Input
                                                    type="email"
                                                    name="email"
                                                    id="exampleEmail"
                                                    placeholder="Email"
                                                    onChange={this.handleChange}
                                                    onKeyDown={this.handleKeyDown}
                                                 
                                                />
                                                </FormGroup>
                                        </Col>
                                        <Col md={12}>
                                        <FormGroup className="mt-2">
                                                    <Label for="examplePassword" className="m-0">Password</Label>
                                                    <Input
                                                        type="password"
                                                        name="password"
                                                        id="examplePassword"
                                                        placeholder="Password "
                                                        onChange={this.handleChange}
                                                        onKeyDown={this.handleKeyDown}
                                                      
                                                    />
                                                </FormGroup>
                                        </Col>
                                    </Row>
                                   
                                </Form>
                              
                               
                            </div>
                            <div className="modal-footer clearfix">                              
                                <div className="float-right">
                                                <Button
                                                        onClick={this.handleLogin}
                                                        onKeyDown={this.handleKeyDown}
                                                        color="primary"
                                                        size="lg"
                                                        >
                                                Acceder</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-center text-black opacity-10 mt-3">
                        Copyright &copy; IdealControl 2019
                    </div>
                </Col>
            </div>
              <div className=""
               
                        style={{
                            backgroundImage: 'url(' + backgroundImage + ')'
                        }}>
                    </div>
        </div>

        </div>
    </Fragment>
    );
    
    }
}



const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
});

const mapDispatchToProps = dispatch => ({
    setNameUsuario: enable => dispatch(setNameUsuario(enable)),
    setEmailUsuario: enable => dispatch(setEmailUsuario(enable)),
    setRolUsuario: enable => dispatch(setRolUsuario(enable))

});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
