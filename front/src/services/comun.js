import axios from 'axios';
import { API_ROOT} from '../api-config';
import moment from 'moment'
const APItag1 = `${API_ROOT}/tag/filteroregister/true`;
//console.log(APItag1);
export class TagServices {    

    getDataTag() { 
        const token = "tokenFalso"; 
        return axios.get(APItag1, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
     }

    getZone(){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/zone/all`
        return axios.get(EndPoint, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data)
    }

    getZoneModulo(name){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/zone/name/${name}`
        return axios.get(EndPoint, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
    }

    getWorkplace(){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/workplace/all`
        return axios.get(EndPoint, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
    }

    getLocation(){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/location/all`
        return axios.get(EndPoint, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
    }

    getLocationsZone(zoneId){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/zone/${zoneId}/location`
        return axios.get(EndPoint, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
    }

    getTagsArray(array){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/tag/array`
        return axios.post(EndPoint,array, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
    }

    updateTags(id,data){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/tag/${id}`
        return axios.put(EndPoint,data, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data)
    }

    getRegistrosToday(array,format,type){
      var date = new Date();
      if(type=="panel_general"){
        date = moment().subtract(15, 'minutes');
      }else{
        date = moment().subtract(6, 'hours');
      }
      var minute = date.minutes();
      let now = new Date(); 
      let f1 = date.format(`${format!=null?format:"YYYY-MM-DDTHH:mm:ss"}`) + ".000Z";
      let f2 = moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
      //console.log(f1)
    
      let f1_split=f1.split("T");
      let f2_split=f2.split("T");
      let EndPoint = "";
      // if(type=="control"){
      //   EndPoint=`${API_ROOT}/measurement-today-control/location/${id}/${f1_split[0]}T${f1_split[1]}/${f2_split[0]}T${f2_split[1]}`
      // }else{
        EndPoint=`${API_ROOT}/measurement/array_tags`
      // }
      
      //console.log(EndPoint)
      let data={
        array:array,
        fini:`${f1_split[0]}T${f1_split[1]}`,
        ffin:`${f2_split[0]}T${f2_split[1]}`
      }
    
      return axios
      .post(EndPoint,data)
      .then(response => response.data.data)
    }

    // tagConfigControlId
    updateTagsConfig(id,data){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/tagConfigControl/${id}`
        return axios.put(EndPoint,data, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data)
    }

    getEndTagsData(data){
      const token = "tokenFalso"; 
      let EndPoint=`${API_ROOT}/measurement/endtags`
        return axios.post(EndPoint,data, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data)
    }

    reformatDate(dateTime){
      var fecha=new Date(dateTime);
      var zona_horaria=new Date(dateTime).getTimezoneOffset();
      zona_horaria=zona_horaria/60;
      fecha.setHours(fecha.getHours()+zona_horaria+1);
      return fecha
    }
}